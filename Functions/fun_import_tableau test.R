fun_import_tableau_test = function(df1, indice, MOT){
    # df = document[ NUM_PAGE == document[INDICE_MOT[1]]$NUM_PAGE ,]
    # indice = INDICE_MOT[8]
    # df1 = document; indice = x; MOT = "TOTAL ASSETS"
    
    # Initiation variable
    EXTRAIRE_MONNAIE = NA_character_
    EXTRAIRE_UNITE   = NA_character_
    EXPORT_INFO = NULL
    
    # TTT 1 : Récupération de la page entière où le mot recherché apparait ===========
    # ================================================================================ #
    df = df1[ NUM_PAGE == df1[indice,]$NUM_PAGE ,]
    
    # TTT 2 : Suppression des lignes après que le mot soit apparu ===========
    # ======================================================================= #
    table1_0b = df[NUM_LIGNE <= (indice +2),]
    
    # country_name <- table1[1] %>% 
    #       stringr::str_squish() %>% 
    #       stringr::str_extract(".+?(?=\\sTotal)")
    # Indentification de la ligne contenant le total asset et suppression de tout ce qu'il y a après ?
    
    # TTT 3 : Exclusion des tableaux HS sur le titre ============
    # =========================================================== #
    # Test ajouté pour le RA 48 p79
    # Exclusion de certain tableau
    tableaux_exclus = c("INTEREST RATE RISK")
    test_titre_interet = table1_0b[1:5,]$TEXT %>% str_detect(paste0(tableaux_exclus, collapse="|")) %>% sapply(function(x) isTRUE(x)) %>% any
    
    if (!test_titre_interet){
        # Ajout pour ra 09 plusieur total assets par page et 2 colonne 
        # table_end <- table1_0 %>% stringr::str_which( MOT ) %>% max
        # table1 <- table1_0[1:(table_end)]
        
        # A verifier : test double colonne 1er test sur 2 total assets sur la même page
        table1_0c = table1_0b
        
        # TTT 4 : Si 2 colonnes et si mot recherché dans la deuxième colonne, suppression de la première colonne ===========
        # ============================================================================================ #
        TEST_DEBUT_MOT = table1_0c[nrow(table1_0c)-2,]$TEXT %>% str_locate(MOT) %>% as.data.frame %>% .$start
        TEST_LONGEUR = lapply(table1_0c$TEXT, nchar) %>% unlist %>% max
        # TEST_2_COLONNES = (TEST_LONGEUR/2) < TEST_DEBUT_MOT
        TEST_2_COLONNES = ( TEST_DEBUT_MOT/TEST_LONGEUR) > 0.45
        
        if( TEST_2_COLONNES ) str_sub(table1_0c$TEXT, 1, TEST_DEBUT_MOT -1) = ""
        
        table1 = paste(table1_0c$NUM_LIGNE, table1_0c$TEXT, sep = "   ")
        
        # TTT 5 : Si plusieurs fois le mot, suppression des lignes traitées dans la boucle précédente ===========
        # ======================================================================================================= #
        indice_plusieurs_mot = table1 %>% stringr::str_which( MOT ) %>% rev %>% .[-1]
        
        if (length(indice_plusieurs_mot)>0) table1 <- table1[-seq(max(indice_plusieurs_mot))]
        
        # TTT 6 : Si un seul espace entre deux parenthèses, ajout d'un espace supplémentaire
        # RA 17 (p129)
        indice_parenthese_space = table1 %>% str_detect("\\) \\(")
        if( indice_parenthese_space %>% any ){
          indice_par_pos = table1[indice_parenthese_space] %>% str_locate("\\) \\(") %>% `+`(1) %>% as.data.frame
          
          for (k in seq(nrow(indice_par_pos)))
              str_sub(table1[ indice_parenthese_space ],  indice_par_pos$start[k] , indice_par_pos$start[k] ) = "  "
        }
        
        # ================================================================================ #
        table1 <- str_replace_all( table1, "\\s{2,}", "|") %>% str_replace_all("^\\|","")
          
        # Consolidation total asset avec espace
        # TEST_ESP_CH1 = (table1[length(table1)] %>% str_replace("[0-9]*\\|","") %>% str_count("(,|\\.)[0-9]{3}") > 1) %>% any
        # TEST_ESP_CH2 = (table1[length(table1)] %>% str_replace("[0-9]*\\|","") %>% str_count("[0-9] [0-9]") > 0) %>% any
        TEST_ESP_CH1 = (table1 %>% str_replace("[0-9]*\\|","") %>% str_count("(,|\\.)[0-9]{3}") > 1)
        TEST_ESP_CH2 = (table1 %>% str_replace("[0-9]*\\|","") %>% str_count("[0-9] [0-9]") > 0)
     
        TEST_ESPACE_ENTRE_CHIFFRE = ( TEST_ESP_CH1 & TEST_ESP_CH2 )
        
        if ( any(TEST_ESPACE_ENTRE_CHIFFRE) ){
          # indice_remplace = table1[ length(table1) ] %>% str_locate_all("[0-9] [0-9]") %>% .[[1]] %>% `+`(1) %>% as.data.frame()
          liste_remplace = table1 %>% str_locate_all("[0-9][0-9]")
         
          for ( ind in seq(liste_remplace) ){
     
              if(TEST_ESPACE_ENTRE_CHIFFRE[i]){
                  indice_remplace = liste_remplace[ind] %>% as.data.frame()
                  if( nrow(indice_remplace)>0){
                      for (k in seq(nrow(indice_remplace)))
                          str_sub(table1[ length(table1) ],  indice_remplace$start[k] +1, indice_remplace$start[k] +1) = "|"
                      }
                  }
              }
          }
          
        
        # Consolidation année avec espace
        # req_date = "(31.?12.?)?20[0-4][0-9]|(31)?.?DEC[^ ]* ?(20)?[0-9]{2}|FY[0-4][0-9]"
        
        req_date = LISTE_DATE %>% paste0(collapse = '|') %>% paste0("(", .,")") 
          
        indice_date = table1 %>% str_which(req_date)
        TEST_ESP_DT = (table1[indice_date] %>% str_count( req_date %>% rep(2) %>% paste0(collapse=' ')) > 0) 
        # TEST_ESP_CH2 = (table1[length(table1)] %>% str_count("[0-9] [0-9]") == 1)
        # TEST_ESPACE_ENTRE_CHIFFRE = ( TEST_ESP_CH1 & TEST_ESP_CH2 )
        #
    
        if (any(TEST_ESP_DT)){
          indice_remplace = table1[ indice_date ][ TEST_ESP_DT ] %>% str_locate_all(req_date) %>% as.data.frame
          for (i in seq(length(indice_remplace))){
            
            indice_remplace1 = table1[ indice_date ][ TEST_ESP_DT ] %>% str_locate_all(req_date) %>% as.data.frame
            
            str_sub(  table1[ indice_date ][ TEST_ESP_DT ]
                    , indice_remplace1[i,]$start 
                    , indice_remplace1[i,]$end ) <- table1[ indice_date ][ TEST_ESP_DT ] %>% 
                                                    str_sub(indice_remplace1[i,]$start , indice_remplace1[i,]$end ) %>%
                                                    str_replace_all( "( |\\.)+", "\\.") %>%
                                                    # Correction 167 (p14)
                                                    str_replace("^\\.", " ")
          }
           #Ajout Correction 167 (p14)
          indice_remplace2 = table1[ indice_date ][ TEST_ESP_DT ] %>% str_locate_all("\\(.*\\)") %>% as.data.frame
          
          if(nrow(indice_remplace2) >0){
              str_sub(  table1[ indice_date ][ TEST_ESP_DT ]
                        , indice_remplace2[1,]$start 
                        , indice_remplace2[1,]$end ) = table1[ indice_date ][ TEST_ESP_DT ] %>% 
                                                        str_sub(indice_remplace2[1,]$start , indice_remplace2[1,]$end ) %>%
                                                        str_replace_all( "( |\\.)+", "\\.")
          }
          table1[ indice_date ][ TEST_ESP_DT ] = table1[ indice_date ][ TEST_ESP_DT ] %>% str_replace_all( " ", "|")
        }
        
        # table2 = mapply( function(x,y) paste(x,y, sep = "|"), x = seq(length(table1)),y = table1)
        table2 = table1
        
        nb_sep = table2 %>% str_count("\\|") 
        nb_sep_max = nb_sep %>% max
        detect_annee = table2 %>% str_detect(req_date)
        
        #Pourquoi le test de la no chaine ?
        detect_no_chaine = table2 %>% 
                           str_replace_all("(JAN|FEB|MAR|APR|MAI|JUN|JUL|AUG|SEP|OCT|NOV|DEC)[^ ]*","") %>% 
                           str_replace_all(paste0( LISTE_MONNAIE, collapse = "|"),"") %>% # Ajout RA 167 p14
                           str_replace_all(paste0( LISTE_UNITE, collapse = "|"),"") %>%   # Ajout RA 167 p14
                           str_count("[:alpha:]") %>%
                           '<'(10)
        
        # conso detect_no_chaine : Ajout test plusieurs dates différentes (RA 167 p14) =====
        detect_many_date =  table2 %>% str_count(req_date) %>% '>'(1)
        detect_no_chaine = detect_no_chaine|detect_many_date
        # =========================
        detect_monnaie = table2 %>% str_count( paste( LISTE_MONNAIE, collapse = "|") ) %>% '>' (0)
        
        # Correction ajout 1 CDC (page 174) : suppression dans la table de la monnaie si elle est dans une phrase
        if ( any( detect_monnaie )){
            test_monnaie_phare = table2[detect_monnaie] %>% 
                        str_replace_all( paste0(   paste0( LISTE_MONNAIE, collapse = "|")
                                                 , paste0( LISTE_UNITE, collapse = "|"), collapse = "|") , "") %>% 
                        str_count("[:alpha:]") %>%
                        `>` (10)
            
            if( any(test_monnaie_phare) ) detect_monnaie[detect_monnaie] = !test_monnaie_phare
          
        }
        
        
        # Test de la colonne TOTAL
        detect_total = rep(FALSE, length(table2))
        
        indic_total = table2 %>% str_which("TOTAL") %>% .[. != length(table2)]
        
        if(length(indic_total)>0){
          
            indic_total = indic_total %>% max
        
            detect_total[indic_total] = TRUE
        }
        
        # Test si chiffre du mot clé est sur la dernière ligne
        detect_last_line = rep(FALSE, length(table2))
        TEST_LAST_LINE = table1[length(table1)]  %>% paste0(collapse='') %>% str_detect("[:digit:]")
        if(TEST_LAST_LINE)  detect_last_line[length(table2)] = TRUE
        
        table2 = table2[  nb_sep == max(nb_sep) | (detect_annee & detect_no_chaine) | detect_total | detect_last_line | detect_monnaie]
        
        table2 = table2 %>% sapply(function(x) { 
          nb_sep_x = str_count(x, "\\|")
          if(  nb_sep_x < nb_sep_max ) x = paste0(x, paste0(rep("|", nb_sep_max - nb_sep_x), collapse = ""))
             x
          }) %>% unlist %>% unname
        
        # consolidation année et monnaie
        #detect_monnaie et (detect_annee & detect_no_chaine)
        # ajout correction pour 52 (page 18)
        # indice_chiffre = length(table2)
        # test_longueur1 = table2[indice_chiffre] %>% str_split("\\|") %>% unlist %>% length %>% `>` (2)
        # if(!test_longueur1 ) {
        #     indice_chiffre = indice_chiffre - 1
        #     test_longueur1 = table2[ indice_chiffre ] %>% str_split("\\|") %>% unlist %>% length %>% `>` (2)
        # }
        
        data_table1 <- table2 %>% textConnection %>% read.csv( sep = "|", header = FALSE, stringsAsFactors = FALSE) %>% setDT
        # names(data_table1)[1] = "NUM"
        
        # suppression de variable avant TOTAL ASSET
        nom_var = names(data_table1)
        indice_var = data_table1[nrow( data_table1),] %>% lapply( function(x) str_detect(x, MOT) ) %>% unlist %>% which(.)
        
        # data_table2_an = data_table1
        data_table2 = data_table1
        
        if(length(indice_var)> 0){ 
            nom_a_supp = nom_var[2:indice_var]
        
            data_table2 = data_table2[, -..nom_a_supp]
        }
        
        # Détection du totalA revérifier
        # if(length(names(data_table2))>1){
        #     test_total = data_table2 %>% str_which("TOTAL") %>% .[.<length(table2)] 
        #  
        #     if( length(test_total) > 0){
        #         test_total = test_total %>% max
        #         vect_total = table2[ test_total ] %>% str_split("\\|") %>%  unlist %>%.[. != ""]
        #         position_total = vect_total %>% str_which("TOTAL")
        #         
        #         variable_select  = data_table2 %>% names 
        #         variable_select2 = variable_select %>% .[-1]
        #         if(position_total == length(vect_total)) variable_select2 = variable_select2 %>% rev
        #         
        #          variable_select2 = c( variable_select[1], variable_select2[1])
        #         
        #          data_table2 = data_table2[,..variable_select2]
        #          
        #     }
        # }
       
        
        # Calcul des valeurs
        # Ajout test détection de la dernière ligne avec une chiffre RA 01 (p4)
      
        # VALEUR = data_table2[ V1 == max(V1) ][,-"V1"] %>% t %>% as.vector %>% .[. != "" ] %>% fun_mnt_num %>% .[!is.na(.)]
        indice_last_line = data_table2[,-"V1"]%>% lapply(function(x){str_which(x,"[0-9]")}) %>% unlist %>% max()
        VALEUR = data_table2[  indice_last_line, ][,-"V1"] %>% t %>% as.vector %>% .[. != "" ] %>% fun_mnt_num %>% .[!is.na(.)]
        
        if ( !all(VALEUR %in% 2000:2100)){
            # FAire un test sur All amounts 145 (p43)
          
            # Recherche de l'unité
            TROUVER_UNITE = table1_0b[str_detect( TEXT , paste( LISTE_UNITE, collapse = "|"))  ]$NUM_LIGNE
            
            # TROUVER_UNITE = table1_0 %>% str_which( paste( LISTE_UNITE, collapse = "|") ) %>%
            #                 .[. <= table_end]
            
            TROUVER_UNITE_TABLE = TROUVER_UNITE %>% .[. %in% data_table1$V1]
            
            if( length(TROUVER_UNITE_TABLE) > 0 ){
                # RA 52 oeb p18 : pb unité sur une seule colonne et pas l'autre
                EXTRAIRE_UNITE = data_table1[ V1 == max(TROUVER_UNITE_TABLE), ][,-"V1"] %>% t %>% as.vector %>% str_subset(paste( LISTE_UNITE, collapse = "|"))
                # EXTRAIRE_UNITE = data_table1[ V1 == max(TROUVER_UNITE_TABLE), ][,-"V1"] %>% t %>% as.vector %>% str_extract( paste( LISTE_UNITE, collapse = "|") ) %>% .[!is.na(.)]
    
              # EXTRAIRE_UNITE = data_table2[ V1 == max(TROUVER_UNITE_TABLE), ][,-"V1"] %>% t %>% as.vector
              # 
              # # test pour 68
              # TEST_MONNAIE = EXTRAIRE_UNITE %>% str_detect( paste( LISTE_MONNAIE  , collapse = "|") ) %>% all
              # if( !TEST_MONNAIE ) EXTRAIRE_UNITE = EXTRAIRE_UNITE %>% str_subset(paste( LISTE_UNITE, collapse = "|"))
              
            }else{
              
                    detect_unite_fin_page = df$TEXT %>% .[length(.)] %>% str_detect( paste0("AMOUNTS.*(", paste0( LISTE_UNITE  , collapse = "|"),')' ))
                    detect_unite_debut_page = df$TEXT %>% .[1:5] %>% str_detect( paste0("AMOUNTS.*(", paste0( LISTE_UNITE  , collapse = "|"),')' )) %>% any
                      
                if ( detect_unite_fin_page|detect_unite_debut_page ){
                    EXTRAIRE_UNITE = NA_character_
                }else{
                    if( length(TROUVER_UNITE)>0 )
                        EXTRAIRE_UNITE = table1_0b[ NUM_LIGNE == max(TROUVER_UNITE) ]$TEXT %>% str_extract( paste( LISTE_UNITE  , collapse = "|") ) %>% .[!is.na(.)]
                }
          
            }
            
            # Recherche monnaie
            # TROUVER_MONNAIE = table1_0 %>% str_which( paste( LISTE_MONNAIE, collapse = "|") ) %>%
            #                 .[. <= table_end]
            TROUVER_MONNAIE = table1_0b[str_detect( TEXT , paste( LISTE_MONNAIE, collapse = "|"))  ]$NUM_LIGNE

            TROUVER_MONNAIE_TABLE = TROUVER_MONNAIE %>% .[. %in% data_table1$V1]
            
            if( length(TROUVER_MONNAIE_TABLE) > 0 ){
                EXTRAIRE_MONNAIE = data_table1[ V1 == max(TROUVER_MONNAIE_TABLE), ][,-"V1"] %>% t %>% as.vector %>% str_subset(paste( LISTE_MONNAIE, collapse = "|"))
                # EXTRAIRE_MONNAIE = data_table1[ V1 == max(TROUVER_MONNAIE_TABLE), ][,-"V1"] %>% t %>% as.vector %>% str_extract( paste( LISTE_UNITE, collapse = "|") ) %>% .[!is.na(.)]
    
            }else{
                
                detect_unite_fin_page = df$TEXT %>% .[length(.)] %>% str_detect( paste0("AMOUNTS.*(", paste( LISTE_MONNAIE  , collapse = "|") ,")"))
                detect_unite_debut_page = df$TEXT %>% .[1:5] %>% str_detect( paste0("AMOUNTS.*(", paste0( LISTE_MONNAIE  , collapse = "|"),")" ) ) %>% any
                      
                
                if ( detect_unite_fin_page ){
                    EXTRAIRE_MONNAIE = df$TEXT %>% .[length(.)] %>% str_subset( paste0( LISTE_MONNAIE  , collapse = "|") )
                }else{
                    if ( detect_unite_debut_page ){
                        EXTRAIRE_MONNAIE = df$TEXT %>% .[1:5] %>% str_subset( paste0( LISTE_MONNAIE  , collapse = "|") ) 
                    }else if( length(TROUVER_MONNAIE)>0 )
                        EXTRAIRE_MONNAIE = table1_0b[ NUM_LIGNE == max(TROUVER_MONNAIE) ]$TEXT %>% str_extract( paste( LISTE_MONNAIE  , collapse = "|") ) %>% .[!is.na(.)]
                }
               
            }
            # correction unité
            # ajout de la correction pour 52 (p18)
            if( isTRUE(TROUVER_MONNAIE_TABLE == TROUVER_UNITE_TABLE)) EXTRAIRE_UNITE = EXTRAIRE_MONNAIE
            
            # TEST 167 plusieurs devises pour le moment ne pas prendre
            if( length(EXTRAIRE_MONNAIE) > 1){
              indice_plusieurs_devises = EXTRAIRE_MONNAIE %>%       
                                         seq() %>% 
                                         sapply( function (x) !str_detect(   EXTRAIRE_MONNAIE[x]
                                                                           , EXTRAIRE_MONNAIE[-x] %>%
                                                                             str_replace_all("\\(","\\\\(") %>%
                                                                             str_replace_all("\\)","\\\\)") %>%
                                                                             str_replace_all("\\$","\\\\$")
                                                                             ) ) %>% 
                                         any()
              
              if (indice_plusieurs_devises) { 
                  VALEUR = NA_real_
                  EXTRAIRE_MONNAIE = NA_character_
              }
            }

            # Recherche année
            # TROUVER_ANNEE = table1_0 %>% str_which( req_date )%>%
            #                   .[. <= table_end]
            TROUVER_ANNEE = table1_0b[ str_detect( TEXT , req_date )  ]$NUM_LIGNE
            
            TROUVER_ANNEE_TABLE = TROUVER_ANNEE %>% .[. %in% data_table1$V1]
            
            if( length(TROUVER_ANNEE_TABLE) > 0 ){
                EXTRAIRE_ANNEE = data_table1[ V1 == max(TROUVER_ANNEE_TABLE), ][,-"V1"] %>% t %>% as.vector%>% str_extract( req_date ) %>% .[!is.na(.)]
            }else{
                # EXTRAIRE_ANNEE = table1_0[ max(TROUVER_ANNEE) ] %>% str_extract( req_date ) %>% .[!is.na(.)]
                EXTRAIRE_ANNEE = table1_0b[ NUM_LIGNE == max(TROUVER_ANNEE) ]$TEXT %>% str_extract( req_date ) %>% .[!is.na(.)]
            }
           # Ajout test 09
            if (length(EXTRAIRE_ANNEE) > length(VALEUR)){
                test_annee = EXTRAIRE_ANNEE %>% str_subset("[0-9]{4}")
                if(length(test_annee) == length(VALEUR)) EXTRAIRE_ANNEE = test_annee
            }
            
            # if ( !( length(EXTRAIRE_ANNEE) == 1 | length(EXTRAIRE_ANNEE) == length(VALEUR)) ) EXTRAIRE_ANNEE = NA_character_
            
            EXPORT_INFO = data.table(  NOM_FICHIER = df$NOM_FICHIER[1]
                                     , TYPE        = MOT
                                     , NUM_PAGE    = df$NUM_PAGE[1]
                                     , NUM_LIGNE   = indice
                                     , DATE        = EXTRAIRE_ANNEE
                                     # , VALEUR      = data_table2[ V1 == table_end ][,-"V1"] %>% t %>% as.vector %>% fun_mnt_num
                                     , VALEUR      = VALEUR
                                     , UNITE_ORI   = EXTRAIRE_UNITE
                                     , MONNAIE     = EXTRAIRE_MONNAIE
                                     )
            EXPORT_INFO = EXPORT_INFO[!is.na(VALEUR)]
            # print(indice)
          }
          
        }
    EXPORT_INFO
}  
  # ajouter supprimer les lignes qui n'ont pas de montant

