fun_import_tableau2 = function(df1, indice, MOT){
    # df1 = document; indice = x;
  
   tt = tryCatch( { 
    # Initiation variable
    EXTRAIRE_MONNAIE = NA_character_
    EXTRAIRE_UNITE   = NA_character_
    EXPORT_INFO = NULL
    
    req_date    = LISTE_DATE    %>% paste0( collapse = '|') %>% paste0("(", .,")") 
    req_unite   = LISTE_UNITE   %>% paste0( collapse = '|')
    req_monnaie = LISTE_MONNAIE %>% paste0( collapse = "|")
    
    partie = "df2"
    # TTT 1 : Si le mot recherché est le titre d'un tableau et chiffre recherché est dans le total ===========
    # ================================================================================ #
    df2 = df1[ NUM_PAGE == df1[NUM_LIGNE == indice,]$NUM_PAGE ,]
    
    MOT_EXTRAIT = df2[ NUM_LIGNE == indice,]$TEXT %>% 
                  str_extract( MOT ) %>% 
                  str_replace_all("^\\s+|(\\s|\t)+([[:digit:],]+|$)|[[:digit:],]+(\\s|\t)+","" )
    
    partie = "df2 supp colonne sur titre"
    # Si multipage avec titre en haut pour différencier
    df2 = fun_supp_colonne_sur_titre(df2, indice, req_date, MOT)
    
    partie = "df2 modifie ligne si TOTAL"
    # Si le mot recherché est le titre du tableau et on prend le TOTAL
    list_df_indice = fun_modifie_ligne_si_total(df2, indice, req_date, MOT)
    
    df0     = list_df_indice $df
    indice = list_df_indice $indice
    
    # Suppression des notes
    df = fun_suppression_note(df0)
    
    # TTT 2 : Suppression des lignes après que le mot soit apparu ===========
    # ======================================================================= #
    # TTT4 : Si le libellé du MOT et les montants ne sont pas sur la même ligne
    # Ajout RA 135 p183 (chiffres sur ligne d'après)
    partie = "table1_0c"
    # Suppresion des lignes
    df[ str_detect(TEXT, "_{2,}"), TEXT := TEXT %>% str_replace_all("_{2,}", "")]
    
    table1_0c = fun_mot_mnt_pas_m_ligne( df, indice, MOT )
    
    
    # TTT 3 : Exclusion des tableaux HS sur le titre ============
    # =========================================================== #
    # Test ajouté pour le RA 48 p79
    # Exclusion de certain tableau
    # tableaux_exclus = c("INTEREST RATE RISK", "KEY SHAREHOLDER DECISIONS")
    # test_titre_interet = table1_0b[1:5,]$TEXT %>% str_detect(paste0(tableaux_exclus, collapse="|")) %>% sapply(function(x) isTRUE(x)) %>% any
    # 
    # if (!test_titre_interet){
        # Ajout pour ra 09 plusieur total assets par page et 2 colonne 
        # table_end <- table1_0 %>% stringr::str_which( MOT ) %>% max
        # table1 <- table1_0[1:(table_end)]
        
    # A verifier : test double colonne 1er test sur 2 total assets sur la même page
    # table1_0b2 = copy(table1_0b)
        
            
    # TTT 4 : Si 2 colonnes et si mot recherché dans la deuxième colonne, suppression de la première colonne ===========
    # ============================================================================================ #
    partie = "table1_0d"
    table1_0d = fun_suppression_1_colonne( table1_0c , MOT_EXTRAIT )
    
    partie = "table1 paste"
    # BOL essayer de le mettre plus loin  
    table1 = paste( table1_0d$ NUM_LIGNE, table1_0d$ TEXT, sep = "   ")
    # table1 = table1_0d$ TEXT
    # TTT 5 : Si plusieurs fois le mot, suppression des lignes traitées dans la boucle précédente ===========
    # ======================================================================================================= #
    partie = "table1 many words"
    table1 = fun_supp_plusieurs_mots( table1, MOT, req_monnaie , req_date, req_unite )
        
    # TTT 6 : Si un seul espace entre deux parenthèses, ajout d'un espace supplémentaire
    # RA 17 (p129)
    partie = "table1 add space"
    table1 = fun_ajt_espace_ent_parent( table1 )
        
    # mettre les dates sur la même ligne ?
    # RA 122 (p44) année sur 2 lignes
    # table1[indice_date]
    partie = "table1 date same line"
    table1 = fun_mettre_date_m_ligne( table1 , req_date, req_monnaie, req_unite )
    
    # ================================================================================ #
    # Suppression des espaces
    partie = "table1b"
    table1b = table1 %>%
              str_replace_all( "\\s{2,}" , "|") %>% 
              str_replace_all( "^\\||\\*", "" )
    
    # Consolidation date
    partie = "table1b consolidation date"
    table1b = fun_consolidation_date( table1b, req_date )
       
    # Détermination des séparations entre chiffre pour les espaces simples
    table1b = fun_supp_espace_ent_chiffre( table1b )
       
    # Consolidation année avec espace
    # req_date = "(31.?12.?)?20[0-4][0-9]|(31)?.?DEC[^ ]* ?(20)?[0-9]{2}|FY[0-4][0-9]"
    # table1b = fun_consolidation_date( table1b, req_date )   
    
    partie = "table2"
    # table2 = fun_select_ligne( table1b, table1, req_date, req_monnaie, req_unite )
    tab_an    = fun_select_ligne( table1b, table1, req_date, req_monnaie, req_unite )
    table2    = tab_an$ df
    ANNEE_MAX = tab_an$ annee_max
    
    # Consolidation monnaie au niveau du mot 
    # RA 19 (p137)
    partie = "table3"
    table3 = fun_conso_monnaie( table2, req_monnaie )
        
    # Ajout deblonnage devise et décalage année RA 64 (p2)
    table3 = fun_decalage_date( table3, req_date )
        
    table3 = fun_decalage_monnaie_date( table3, req_monnaie, req_date )  
        
    # Ajout séparateur entre deux chiffres
    table3 = fun_ajout_sep_num_3_1_3( table3 )
        
    # Ajout séparateur entre monnaie BOL
    table3 = fun_ajout_sep_2_monnaie( table3, req_monnaie )
    
    # Suppression NOTE
    table3 = table3 %>% str_replace("NOTE\\|","") %>% str_replace_all("\"", "\'")
    
    # Suppression NOTE en chiffre
    table3 = fun_suppresion_note_chiffre( table3 )
  
    
    partie = "table4"
    # Ajout le même nombre de spéarateur par ligne
    table4 = fun_uniform_sep_ligne( table3 )
    
    # Bug RA 32
    # Transformatione en DF
    partie = "data_table1"
    data_table1 = fun_vect_to_dt( table4 )
    
    # suppression de variable avant TOTAL ASSET
    partie = "data_table2"
    data_table2 = fun_supp_var( data_table1, MOT)
    
    # Calcul des valeurs
    # Ajout test détection de la dernière ligne avec une chiffre RA 01 (p4)
    partie = "VALEUR"
    VALEUR = fun_recup_valeur( data_table2, req_monnaie, req_date )
    
    partie = "EXTRAIRE_MONNAIE"
    # FAire un test sur All amounts 145 (p43)
    EXTRAIRE_MONNAIE = fun_recup_unite( table1_0c, df, data_table1, req_monnaie, VALEUR )
    
    # TEST 167 plusieurs devises pour le moment ne pas prendre
    TEST_MULTI_DEVISE = fun_multi_devise( VALEUR, EXTRAIRE_MONNAIE )
      
    if ( TEST_MULTI_DEVISE ) {
        VALEUR = NA_real_
        EXTRAIRE_MONNAIE = NA_character_
    }
    
    partie = "EXTRAIRE_UNITE"
    EXTRAIRE_UNITE = fun_recup_unite( table1_0c, df, data_table1, req_unite, VALEUR, EXTRAIRE_MONNAIE )
    
    # correction unité
    # ajout de la correction pour 52 (p18)

    # Recherche année
    # EXTRAIRE_ANNEE = fun_recup_annee(table1_0c, data_table1, req_date, ANNEE_MAX, VALEUR )
    partie = "EXTRAIRE_ANNEE"
  
    EXTRAIRE_ANNEE = fun_recup_annee( table1, data_table1, req_date, ANNEE_MAX, VALEUR )
    
    # Suppression si beaucoup de valeur et pas beacoup d'année
    test_nb_valeur =  VALEUR %>% .[!is.na(.)] %>% length
    test_nb_annee =   EXTRAIRE_ANNEE %>% .[!is.na(.)] %>% length
    if ((test_nb_valeur > 2 &  test_nb_annee/test_nb_valeur< 0.5)){
      VALEUR = NA_real_
    }
    
    # Ajout 21 (p92) plusieurs monnaie et plusieur unité
    TEST_UNICITE_MONNAIE = EXTRAIRE_MONNAIE %>% str_extract( req_monnaie ) %>% unlist %>% str_replace_all(" ", "") %>% unique %>% .[!is.na(.)]
    TEST_UNICITE_UNITE   = EXTRAIRE_UNITE   %>% str_extract( req_monnaie ) %>% unlist %>%.[!is.na(.)] %>% str_replace_all(" ", "") %>% unique %>% .[!is.na(.)]
  
    if ( (length(unique(TEST_UNICITE_MONNAIE) ) > length(TEST_UNICITE_UNITE)) & length(TEST_UNICITE_UNITE)==1 ){
        UNITE_UNIQUE   = EXTRAIRE_UNITE %>% unique %>% .[!is.na(.)]
        EXTRAIRE_UNITE = rep( UNITE_UNIQUE, length(EXTRAIRE_MONNAIE))
        
        T1 = sapply( EXTRAIRE_MONNAIE , function(y) sapply( LISTE_MONNAIE_L, function(x)  y %>% str_detect( paste0(x, collapse='|')) %>% any ) %>% which)
        T2 = sapply( EXTRAIRE_UNITE   , function(y) sapply( LISTE_MONNAIE_L, function(x)  y %>% str_detect( paste0(x, collapse='|')) %>% any ) %>% which)
        
        if ( isTRUE(any(T1!=T2))) EXTRAIRE_UNITE[T1!=T2] = NA_character_
    }
    
    partie = "EXPORT_INFO"
    if ( (VALEUR %>% is.na %>% `!` %>% any)  & !( length( VALEUR )> 7 & n_distinct( EXTRAIRE_ANNEE ) == 1 )   ){
      
        EXPORT_INFO = data.table(  NOM_FICHIER = df$ NOM_FICHIER[1]
                                 , TYPE        = MOT
                                 , MOT_EXTRAIT = MOT_EXTRAIT
                                 , NUM_PAGE    = df$NUM_PAGE[1]
                                 , NUM_LIGNE   = indice
                                 , DATE        = EXTRAIRE_ANNEE
                                 , VALEUR      = VALEUR
                                 , UNITE_ORI   = EXTRAIRE_UNITE
                                 , MONNAIE     = EXTRAIRE_MONNAIE 
                                 )
      
        EXPORT_INFO = EXPORT_INFO[ !is.na(VALEUR) ]
      
    }           

    
    EXPORT_INFO
   }
, error=function(e) print(paste("error sur ", indice, ", partie : ",   partie))
# , warning = function(w)  print(paste("warning sur ", indice,", partie : ",   partie))
)
}  
  # ajouter supprimer les lignes qui n'ont pas de montant

