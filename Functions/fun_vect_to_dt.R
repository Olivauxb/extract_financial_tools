fun_vect_to_dt = function(vect){
  out = vect
  
  if (length(out)>0){
    out = out %>% 
          textConnection %>% 
          read.csv( sep = "|", header = FALSE, stringsAsFactors = FALSE) %>% 
          setDT
  }else{
    out = as.data.table(out)
  }
  
  out
}