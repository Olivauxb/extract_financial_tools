fun_multi_devise = function( VALEUR, EXTRAIRE_MONNAIE ){
  
  INDICE = FALSE
  
  if( length( EXTRAIRE_MONNAIE ) > 1 & length( EXTRAIRE_MONNAIE ) < length( VALEUR ) ){
    
      EXTRAIRE_MONNAIE2 = EXTRAIRE_MONNAIE %>% unique()
      
      INDICE = EXTRAIRE_MONNAIE2 %>% 
               seq() %>% 
               sapply( function (x) !str_detect(  EXTRAIRE_MONNAIE2[ x] 
                                                , EXTRAIRE_MONNAIE2[-x] %>%
                                                  str_replace_all("\\(","\\\\(") %>%
                                                  str_replace_all("\\)","\\\\)") %>%
                                                  str_replace_all("\\$","\\\\$") %>%
                                                  str_replace_all("[ [0-9],\\.]", "") %>% 
                                                  paste0( collapse ="|" ) )
               ) %>% any()  
  }
  INDICE
}

