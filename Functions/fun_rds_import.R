fun_rds_import = function( i, LISTE_TOTAL = LISTE_TOTAL_MOTS , PAGES_A_EXCLURE = PAGES_TITRES_A_EXCLURE , RECHERCHE_MOT_DANS_TITRE = NULL){
# i = LISTE_RDS[1]
# EXPORT_DATA = fun_import_tableau( document, x, LISTE_MOT , RECHERCHE_MOT_DANS_TITRE )

  tt = tryCatch( {
req_date    = LISTE_DATE    %>% paste0( collapse = '|') %>% paste0("(", .,")") 
req_unite   = LISTE_UNITE   %>% paste0( collapse = '|')
req_monnaie = LISTE_MONNAIE %>% paste0( collapse = "|")

document = readRDS(i)

LISTE_MOT_INDICE = lapply( LISTE_TOTAL_MOTS, function(x){
  
     # MOT = paste0( LISTE_TOTAL_MOTS[[3]], collapse='|')
     MOT = paste0( x, collapse='|')
     # print(MOT)
          # MOT = paste0( unlist(LISTE_TOTAL), collapse='|')
     INDICE_MOT = document$TEXT %>% grep( MOT,. , perl =TRUE)
})

 
# tes$TEXT %>% str_detect('([^[:alpha:]]{3,}|^[[:blank:]]*([IVX]+.[[:blank:]]*)?)NET PROFIT. .LOSS. FOR THE YEAR(([[:blank:]]+[^[:blank:]]?[[:blank:]]*)([[:digit:]]|$)|$)')
df7=NULL
if ( LISTE_MOT_INDICE %>% sapply( length ) %>% max %>% `>`(0)){
dt_t_g0 = LISTE_MOT_INDICE %>%
         names %>%
         # length %>% 
         # seq %>%
         lapply( function(y){ 
            lapply( LISTE_MOT_INDICE[[y]], function(x){ 
                MOT_INTER = document[ NUM_LIGNE == x]$TEXT %>% str_extract(paste0( LISTE_TOTAL_MOTS[[y]], collapse='|'))
                document[ NUM_PAGE %in% document[x]$NUM_PAGE ][,":="( MOT_RECHERCHE = y , ID_INDICE = x, MOT_EXTRAIT = MOT_INTER, MOT_ER =  paste0( LISTE_TOTAL_MOTS[[y]], collapse='|'))]
              # }) %>% rbindlist()
         #      LISTE_MOT_INDICE[[y]][x]
             }) %>% rbindlist()
         })%>% rbindlist()

dt_t_g0[,':='(NEW_NUM_LIGNE = 1:.N)]

# dt_t_g0[ NUM_LIGNE == ID_INDICE,':='(ID_INDICE_NEW = NEW_NUM_LIGNE)]
NEW_INDICE_MOT = dt_t_g0[ NUM_LIGNE == ID_INDICE ]$NEW_NUM_LIGNE

dt_t_g = copy(dt_t_g0)

if(!is.null(RECHERCHE_MOT_DANS_TITRE)){
  dt_t_g[ ,':='( RECHERCHE_MOT_DANS_TITRE = TEXT %>% str_extract_all( paste0( RECHERCHE_MOT_DANS_TITRE, collapse ="|")) %>% sapply(function(x) x %>% .[. !=""] %>% paste0(collapse="|")))
    ][ RECHERCHE_MOT_DANS_TITRE != "",':='( RECHERCHE_MOT_DANS_TITRE = paste0(RECHERCHE_MOT_DANS_TITRE, collapse ="|")), by = .(MOT_RECHERCHE, ID_INDICE)
    ][                               ,':='( RECHERCHE_MOT_DANS_TITRE = max(RECHERCHE_MOT_DANS_TITRE)), by = .(MOT_RECHERCHE, ID_INDICE)
    ]
}else{
  dt_t_g[ ,':='( RECHERCHE_MOT_DANS_TITRE = "")]
}

dt_t_g = dt_t_g [, ':=' (IND = 1:.N ), by =.( MOT_RECHERCHE, ID_INDICE )
                  
                  # test 0 : Détection des pages à exclures sur le titre
                  ][ IND %in% 1:5, ":=" (base_t_0 = TEXT %>% str_detect(paste0(PAGES_A_EXCLURE, collapse="|"))
                                        )
                     
                  # test 1 :
                  # test 2 : 
                  # test 4a: text de longeur 1
                  ][ NEW_NUM_LIGNE %in% NEW_INDICE_MOT,':='( base_t_1 = TEXT %>% 
                                                                        str_sub( str_locate(., MOT_ER)[,2]+1, nchar(.)) %>% 
                                                                        str_replace_all("^\\s+|\\s+$", "") %>% 
                                                                        paste0("  ") %>% 
                                                                        fun_test_si_phrase2
                    
                                                    , base_t_2 = TEXT %>% 
                                                                 str_sub( str_locate(., MOT_ER)[,2]+1, nchar(.)) %>% 
                                                                 str_replace_all("^\\s+|\\s+$", "") %>% 
                                                                 paste0("  ") %>% 
                                                                 fun_test_many_elements2
                                           
                                                    , IND_5_6 = IND
                                                    , base_t_4_a = TEXT %>% str_replace_all("^\\s+|\\s+$", "") %>% str_split('\\s{2,}') %>% sapply(length) %>% `%in%`(1)
                                                    
                                                    )
                     
                  ][ NEW_NUM_LIGNE %in% c( NEW_INDICE_MOT-1, NEW_INDICE_MOT+1 ), ":="( base_t_3 = TEXT %>% fun_test_si_phrase2
                                                                          , base_t_4_b = TEXT %>% str_replace_all("^\\s+|\\s+$", "") %>% str_split('\\s{2,}') %>% sapply(length) %>% `>`(1)
                                                                          )
                     
                     
                  ][, ':=' (   IND_5 = min( IND_5_6, 9999999999, na.rm=TRUE)
                             , IND_6 = max( IND_5_6,          0, na.rm=TRUE)
                             ), by =.(MOT_RECHERCHE, ID_INDICE)
                    
                  ][ str_detect( TEXT,'TOTAL( A .I . II . III.|[^[:alpha:]]{2,})' )
                    & IND_5 > IND , ":=" ( base_t_5 = TEXT %>% str_count("[:digit:]") %>% `>` (3))
                  ][  IND_6 < IND , ":=" ( base_t_6 = TEXT %>% str_count("3 MONTHS OR|TO [0-9] YEAR|LEVEL [1-3]|OVER [0-9]|MONTHS|YEARS"))
                        
                   
                  ][,':='(   TEST_0 = base_t_0 %>% any %>% isTRUE %>% `!`
                           , TEST_1 = base_t_1 %>% any %>% isTRUE
                           , TEST_2 = base_t_2 %>% any %>% isTRUE
                           , TEST_3 = base_t_3 %>% any %>% isTRUE
                           , TEST_4 = isTRUE( any( base_t_4_a )) & isTRUE( any( base_t_4_b )) 
                           , TEST_5 = base_t_5   %>% any %>% isTRUE
                           , TEST_6 = base_t_6   %>% sum(na.rm = TRUE) %>% `<` (4) %>% any %>% isTRUE
                          
                    ), by= .(MOT_RECHERCHE, ID_INDICE)
                  # ][,':='( TEST_4 =  TEST_4_a & TEST_4_b )
                  ][ ,":=" ( TEST_F = TEST_0  & ( TEST_1 | (TEST_2 & ( (TEST_3 & TEST_4) | TEST_5 ))) & TEST_6 )]


PERIM = dt_t_g[TEST_F == TRUE]
#PERIM = dt_t_g[TEST_F == TRUE & MOT_RECHERCHE == "PROFIT"]

# MOT = paste0( LISTE_MOT[[1]], collapse='|')

EXTRAIRE_MONNAIE = NA_character_
EXTRAIRE_UNITE   = NA_character_
EXPORT_INFO = NULL


PERIM[ NUM_LIGNE == ID_INDICE
       
       , MOT_EXTRAIT := TEXT %>% 
                        str_extract( MOT_ER ) %>% 
                        str_replace_all("^\\s+|(\\s|\t)+([[:digit:],]+|$)|[[:digit:],]+(\\s|\t)+","" )]

PERIM[ , MOT_EXTRAIT := max(MOT_EXTRAIT, na.rm = TRUE), by=.(MOT_RECHERCHE,ID_INDICE)]

PERIM[  NUM_LIGNE == ID_INDICE
        , ":="( POS_MOT_E = str_locate(TEXT, MOT_ER) %>% as.data.frame %>% .$end)
     ][ , ":="( POS_MOT_E = POS_MOT_E %>% max(na.rm=TRUE) ), by =.(MOT_RECHERCHE,ID_INDICE)
     ]

partie = "df2 supp colonne sur titre"
# df = PERIM[IND == 1][1:5]
df = copy(PERIM)
#df [IND == 1 , NBR_TITRES := TEXT %>%
df [ IND == 1, NBR_TITRES := TEXT %>% 
  # str_replace_all("^\\s+|^\\s+$","") %>%
  strsplit("\\s{2,}") %>% 
  # unlist %>% 
  sapply( function(x) { x %>% 
                        str_replace_all( "(^| )[:digit:]+($| )", "") %>% 
                        str_replace_all(req_date, "") %>%
                        str_replace_all("NOTES", "") %>% 
                        # .[. %!in% c("", "..")] %>% 
                        length
  })]
  # 
df [, LONGUEUR_MAX := TEXT %>% str_length %>% max, by =.(MOT_RECHERCHE,ID_INDICE) 
    
  ][ NBR_TITRES == 2, ":="(  MOT1 = word(TEXT, 1, sep="\\s{2,}")
                           , MOT2 = word(TEXT, 2, sep="\\s{2,}") ) 

  ][ NBR_TITRES == 2, ":="(   
         TITRE_M1_B = TEXT %>% str_locate(MOT1) %>% as.data.frame %>% .$start
       , TITRE_M1_E = TEXT %>% str_locate(MOT1) %>% as.data.frame %>% .$end
       , TITRE_M2_B = TEXT %>% str_locate(MOT2) %>% as.data.frame %>% .$start
       , TITRE_M2_E = TEXT %>% str_locate(MOT2) %>% as.data.frame %>% .$end
    )
  ][ NBR_TITRES == 2, MILIEU := ifelse(  (LONGUEUR_MAX - TITRE_M2_E) < 15 
                                          , TITRE_M2_E 
                                          , LONGUEUR_MAX /2)
       
  ][, ":="(  NBR_TITRES = NBR_TITRES %>% max( na.rm = TRUE )
           , TITRE_M1_B = TITRE_M1_B %>% max( na.rm = TRUE )
           , TITRE_M1_E = TITRE_M1_E %>% max( na.rm = TRUE )
           , TITRE_M2_B = TITRE_M2_B %>% max( na.rm = TRUE )
           , TITRE_M2_E = TITRE_M2_E %>% max( na.rm = TRUE )
           , MILIEU     = MILIEU     %>% max( na.rm = TRUE )
           
            ), by = .(MOT_RECHERCHE, ID_INDICE)
    
  ][  NBR_TITRES == 2 
    & (TITRE_M1_B/MILIEU)<0.8
       
     , TEXT := case_when (   TITRE_M1_E < MILIEU & MILIEU < TITRE_M2_B 
                           & POS_MOT_E  < MILIEU                        ~ str_sub( TEXT, 1     , MILIEU       )
                         ,   TITRE_M1_E < MILIEU & MILIEU < TITRE_M2_B  ~ str_sub( TEXT, MILIEU, LONGUEUR_MAX )
                         ,   TITRE_M1_B < MILIEU & TITRE_M2_B < MILIEU
                           & ( TEXT %>% 
                                  str_sub(1, TITRE_M2_E) %>% 
                                  str_count("^[0-9]\\.[0-9] ") %>% 
                                  sum %>% `>` (6) )                      ~ str_sub( TEXT, TITRE_M2_E, LONGUEUR_MAX)
                                )
   
    ]

partie = "df2 modifie ligne si TOTAL"
# Si le mot recherché n'a pas chiffre qui suit et on prends les chiffres de la ligne suivante avec TOTAL si existant
# 1) test si pas de chiffre sur la ligne mot (peutêtre déterminer colonne avant ?)
dfb = copy(df)
dfb [, FLAG_REMPLACE := 0]

dfb[ NUM_LIGNE == ID_INDICE ,':=' ( TEST_IND_VIDE = TEXT %>% str_replace_all(req_date, "") %>% str_count('[:digit:]') == 0)
  
  ][ , ":=" ( TEST_IND_VIDE = TEST_IND_VIDE %>% max( na.rm = TRUE)), by = .(MOT_RECHERCHE,ID_INDICE)
  ]

express_total = "(^ ?|[^[:alpha:]]{2,})TOTAL( A .I . II . III.)?[^[:alpha:]]*[:digit:]"

dfb[, ":="(TEST_ANALYSIS = NA_integer_, TEST_TOTAL = NA_integer_, ID_INDICE_PROVISOIR = NA_integer_ )]
dfb[ TEST_IND_VIDE == TRUE & NUM_LIGNE > ID_INDICE & TEXT %>% str_detect("ANALYSIS OF"),":="( TEST_ANALYSIS = NUM_LIGNE)
  ][ TEST_IND_VIDE == TRUE & NUM_LIGNE > ID_INDICE & TEXT %>% str_detect(express_total),":="( TEST_TOTAL    = NUM_LIGNE)
  ]
dfb[ , ":=" (  TEST_ANALYSIS = TEST_ANALYSIS %>% min( na.rm = TRUE)
              , TEST_TOTAL    = TEST_TOTAL  %>% min( na.rm = TRUE)
              ), by=.(MOT_RECHERCHE,ID_INDICE)
  ]
dfb[ is.na(TEST_ANALYSIS)|is.infinite(TEST_ANALYSIS), ":="(TEST_ANALYSIS = 0)]
dfb[ is.na(TEST_TOTAL ) |is.infinite(TEST_TOTAL) , ":="(TEST_TOTAL =0 )]

dfb[ TEST_IND_VIDE == TRUE & TEST_TOTAL < TEST_ANALYSIS & NUM_LIGNE == TEST_TOTAL 
     , ":="(  TEXT = paste( MOT_EXTRAIT, str_replace( TEXT, "TOTAL( A .I . II . III.)?", ""))
            , ID_INDICE_PROVISOIR = NUM_LIGNE
            , FLAG_REMPLACE = 1
     )
  ][ , ":=" (  ID_INDICE_PROVISOIR = ID_INDICE_PROVISOIR %>% min(na.rm = TRUE)
            ), by=.(MOT_RECHERCHE,ID_INDICE)
    
  ][ is.infinite(ID_INDICE_PROVISOIR)|is.na(ID_INDICE_PROVISOIR), ':='(ID_INDICE_PROVISOIR = ID_INDICE)
  ][ TEST_TOTAL < TEST_ANALYSIS &  TEST_TOTAL > 0 & TEST_IND_VIDE == TRUE & NUM_LIGNE == ID_INDICE, TEXT := ""
  # ][ TEST_TOTAL < TEST_ANALYSIS & TEST_TOTAL  
  ]

setorder(dfb, MOT_RECHERCHE, ID_INDICE_PROVISOIR, ID_INDICE, NUM_LIGNE)
dfb[,":="( c= (1:.N == 1)*1), by = .(MOT_RECHERCHE,ID_INDICE)]
dfb[, ":="(c= cumsum(c)), by = .(MOT_RECHERCHE,ID_INDICE_PROVISOIR)]
dfb= dfb[c==1]
dfb[ ID_INDICE != ID_INDICE_PROVISOIR, ":="( ID_INDICE = ID_INDICE_PROVISOIR )]
#Ajout suppression ligne mot

# df[,c( "POS_MOT_E", "NBR_TITRES", "LONGUEUR_MAX", "MOT1", "MOT2", "TITRE_M1_B", "TITRE_M1_E", "TITRE_M2_B", "TITRE_M2_E", "c"
#        ,"ID_INDICE_PROVISOIR" ,"TEST_ANALYSIS", "TEST_TOTAL" ):= NULL]
df2= copy(dfb)
df2[, ':='(TEST_PAS_CHIFFRE = FALSE, TEST_L_P_1_CHIFFRE = FALSE, TEST_L_M_1_CHIFFRE = FALSE, ID_INDICE_PROVISOIR = NA_integer_)
   
][ NUM_LIGNE == ID_INDICE & str_count(TEXT, "[0-9]")==0, ":="(TEST_PAS_CHIFFRE = TRUE)

]

df2[, ":=" (   TEST_PAS_CHIFFRE   = TEST_PAS_CHIFFRE  %>% max(na.rm = TRUE)  ), by=.(MOT_RECHERCHE,ID_INDICE)
  
# ][TEST_PAS_CHIFFRE == TRUE & NUM_LIGNE == (ID_INDICE-1) & str_count(TEXT, "[0-9]")>0 & str_count(TEXT, "[:alpha:]")==0, ":="( TEST_L_M_1_CHIFFRE = TRUE)
][TEST_PAS_CHIFFRE == TRUE & NUM_LIGNE == (ID_INDICE+1) & str_count(TEXT, "[0-9]")>0 & str_count(TEXT, "[:alpha:]")==0, ":="( TEST_L_P_1_CHIFFRE = TRUE)

][ , ":=" (   TEST_L_M_1_CHIFFRE = TEST_L_M_1_CHIFFRE %>% max(na.rm = TRUE)
            , TEST_L_P_1_CHIFFRE = TEST_L_P_1_CHIFFRE %>% max(na.rm = TRUE)
          ), by=.(MOT_RECHERCHE,ID_INDICE)
   
][  (TEST_PAS_CHIFFRE == TRUE & TEST_L_P_1_CHIFFRE == TRUE & NUM_LIGNE == (ID_INDICE+1) )
  | (TEST_PAS_CHIFFRE == TRUE & TEST_L_P_1_CHIFFRE == FALSE & NUM_LIGNE == (ID_INDICE-1) & TEST_L_M_1_CHIFFRE == TRUE)
   
   , ":="(  TEXT = paste( MOT_EXTRAIT,  TEXT)
          , ID_INDICE_PROVISOIR = NUM_LIGNE
          , FLAG_REMPLACE = 1
          )
   
][ , ":=" (  ID_INDICE_PROVISOIR = ID_INDICE_PROVISOIR  %>% min(na.rm = TRUE)
), by=.(MOT_RECHERCHE,ID_INDICE)

][ is.infinite(ID_INDICE_PROVISOIR)| is.na(ID_INDICE_PROVISOIR), ':='(ID_INDICE_PROVISOIR = ID_INDICE)
   
][ TEST_PAS_CHIFFRE == TRUE & ( TEST_L_P_1_CHIFFRE == TRUE| TEST_L_M_1_CHIFFRE == TRUE) & NUM_LIGNE == ID_INDICE, TEXT := ""

# ][ ID_INDICE != ID_INDICE_PROVISOIR, ":="( ID_INDICE = ID_INDICE_PROVISOIR )
   
]
# ajout consol + suppression mot
# Suppression doublon pour chiffre entre deux mots
setorder(df2, MOT_RECHERCHE, ID_INDICE_PROVISOIR, ID_INDICE, NUM_LIGNE)
df2[,":="( c= (1:.N == 1)*1), by = .(MOT_RECHERCHE,ID_INDICE)]
df2[, ":="(c= cumsum(c)), by = .(MOT_RECHERCHE,ID_INDICE_PROVISOIR)]
df2b= df2[c==1]
df2b[ ID_INDICE != ID_INDICE_PROVISOIR, ":="( ID_INDICE = ID_INDICE_PROVISOIR )]


# 1ère suppression des notes
df3 = copy(df2b)

df3[, ":="( TEST_NOTE = str_detect( TEXT, "(  |\t)NOTE(  |\t)") )
  ][, ':='( TEST_NOTE = TEST_NOTE %>% max), by=.(MOT_RECHERCHE,ID_INDICE)]

df3[ TEST_NOTE == TRUE, TEXT := TEXT %>% str_replace("  [0-9]{1,2} ?(\\( ?[[:alpha:]] ?\\))?  ", '') %>%
                                         str_replace("(  |\t)NOTES?(  |\t)", '')]

# suppression des tirets
df3[ str_detect(TEXT, "_{2,}"), TEXT := TEXT %>% str_replace_all("_{2,}", "")]

# Si les chiffres sont sur des lignes différentes comparaison par rapport à la date
df3[ NUM_LIGNE <= (ID_INDICE + 1) , ":="( LONGEUR_DATE = TEXT %>% str_count(req_date) )
  ][ NUM_LIGNE ==  ID_INDICE      , ":="( NOMBRE_C_1 = TEXT %>% str_count("[0-9]+")  )
  ][ NUM_LIGNE == (ID_INDICE + 1) , ":="( NOMBRE_C_2 = TEXT %>% str_count("[0-9]+")  )
     
  ][, ':='(   LONGEUR_DATE = LONGEUR_DATE %>% max(0,na.rm=TRUE)
            , NOMBRE_C_1   = NOMBRE_C_1   %>% max(0,na.rm=TRUE)
            , NOMBRE_C_2   = NOMBRE_C_2   %>% max(0,na.rm=TRUE)
            , TEXT_LEAD    = TEXT %>% lead()
            ), by=.(MOT_RECHERCHE,ID_INDICE)
    
  ][ LONGEUR_DATE == 2 & NOMBRE_C_1 == 1 & NOMBRE_C_2 == 1 & NUM_LIGNE ==  ID_INDICE    , TEXT := paste(str_trim(TEXT), TEXT_LEAD, sep = "  ")
  ][ LONGEUR_DATE == 2 & NOMBRE_C_1 == 1 & NOMBRE_C_2 == 1 & NUM_LIGNE ==  (ID_INDICE+1), TEXT := "" 
  ]

# Suppression des paragraphe par rapport à la position du mot par rapport à la page
#df4[,-c("MOT_ER")][NUM_LIGNE == ID_INDICE]
df4 = copy(df3)
df4[ , ":="( POS_MOT_B = 0, POS_MOT_E = 0, LOCATE_GD_ESPACE =0 )]
df4[  NUM_LIGNE == ID_INDICE , ":="(   POS_MOT_B = str_locate(TEXT, MOT_ER) %>% as.data.frame %>% .$start
                                     , POS_MOT_E = str_locate(TEXT, MOT_ER) %>% as.data.frame %>% .$end   )
      
  ][ , ":="(   LONGUEUR_LIGNE = TEXT %>% str_length )    
  ][ , ":="(   POS_MOT_B    = POS_MOT_B %>% max(na.rm=TRUE)
             , POS_MOT_E    = POS_MOT_E %>% max(na.rm=TRUE)
             , LONGUEUR_MAX = LONGUEUR_LIGNE %>% max(na.rm=TRUE)
             , NB_LIGNES    = .N 
            ), by =.(MOT_RECHERCHE,ID_INDICE)
  
  # Si plus de 10 lignes et le début du mot recherché est au moins à 20% de la page global   
  ][ NB_LIGNES > 10 & (POS_MOT_B> 0.2*LONGUEUR_MAX ), TEXT := TEXT %>% str_sub( POS_MOT_B, LONGUEUR_LIGNE )
    
  # Si détection d'un grand espace sur la ligne du mot recherché 
  ][ NB_LIGNES > 10 &  NUM_LIGNE == ID_INDICE & !str_detect(TEXT,"\t"), ':='(LOCATE_GD_ESPACE = TEXT %>% str_locate('[:digit:]\\s{80,}') %>% as.data.frame %>% .$start %>% `+`(1))

  ][ , ":="(   LOCATE_GD_ESPACE    = LOCATE_GD_ESPACE %>% max(na.rm=TRUE)
            ), by =.(MOT_RECHERCHE,ID_INDICE)
     
  ][ LOCATE_GD_ESPACE > 0, TEXT := TEXT %>% str_sub( 1, LOCATE_GD_ESPACE)
    
  ][ , ':='( NB_TAB = 0, LONGUEUR_LIGNE = TEXT %>% str_length, POST_CHIFFRE = 0 )
    
  ][  NB_LIGNES > 10 & NUM_LIGNE == ID_INDICE , ":="(   POST_CHIFFRE = TEXT %>% str_locate('[0-9][^[:alpha:]]+') %>% as.data.frame %>% .$end
                                                      , NB_TAB = TEXT %>% str_count("\t")
                                                    )
  
  ][ , ":="(  POST_CHIFFRE   = POST_CHIFFRE   %>% max(na.rm=TRUE)
            , LONGUEUR_LIGNE = LONGUEUR_LIGNE %>% max(na.rm=TRUE)
            , NB_TAB         = NB_TAB         %>% max(na.rm=TRUE)
            ), by =.(MOT_RECHERCHE,ID_INDICE)
    
  ][ NB_TAB == 0 & ( POST_CHIFFRE < 0.7*LONGUEUR_LIGNE ), ':='(TEXT = str_sub( TEXT, 1, POST_CHIFFRE))
  ]

df5 = copy(df4)
df5 = df5 [,-c("MOT_ER")]  
df5[, ':='(TEXT = TEXT %>% 
                  str_replace_all( "\\) ",")  ") %>% 
                  str_replace_all( "\\) ?\\(",")  (") %>%
                  str_replace_all("WERE APPROVED ON .*20[0-9]{2}.BY THE BOARD OF DIRECTORS", "") %>%
                  str_replace_all("\"", "\'") )]

# Ajout espace pour séparateur si pas assez d'espace entre 2 chiffres
nb_boucles = df5[NUM_LIGNE == ID_INDICE]$TEXT %>% str_count("[0-9]{3} [0-9]{1,2} [0-9]{3}") %>% max

df5[, ":="(pos=0, long=0)]
if (nb_boucles >0){
  lapply( seq(nb_boucles), function (x){
    
        df5[  NUM_LIGNE == ID_INDICE & str_detect( TEXT,"[0-9]{3} [0-9]{1,2} [0-9]{3}"), ':='(  pos  = TEXT %>% str_locate("[0-9]{3} [0-9]{1,2} [0-9]{3}")%>% as.data.frame %>% .$start %>% `+`(3)
                                                                                              , long = TEXT %>% str_length )
          ][  pos > 0, ':='(  TEXT = paste0( str_sub( TEXT, 1, pos) , str_sub( TEXT, pos, long )))
          ]
        
        if ( any(names(df5) %in% c("pos", "long") ))
            df5[, c("pos", "long"):= NULL]
  }
  )
}
if (any(c("pos", "long") %in% names(df5))) df5[, ":="(pos=NULL, long=NULL)]

# Récupération des valeurs
df5[  NUM_LIGNE == ID_INDICE, ':='(VALEUR = TEXT %>% 
                                            str_replace_all("\\(", '-') %>%
                                            str_replace( req_monnaie, "") %>% 
                                            str_replace( req_date   , "") %>% 
                                            str_replace_all("[:alpha:]|[:digit:]*%|\\)","") %>%
                                            str_replace_all("\\s{2,}", "  ") %>%
                                            str_replace_all("\\s{2,}-\\s{2,}", "  0  ")
  ) ]

df5[  NUM_LIGNE == ID_INDICE & str_detect( VALEUR, "[0-9]{3}\\.[0-9]{3}")   , ":="( VALEUR = VALEUR %>% str_replace_all("\\.", "")) 
  ][  NUM_LIGNE == ID_INDICE & str_detect( VALEUR, "[0-9]{3},[0-9]{3}")     , ":="( VALEUR = VALEUR %>% str_replace_all(",", ""))
  ][  NUM_LIGNE == ID_INDICE & str_detect( VALEUR, "[0-9]{3},[0-9]{2}( |$)"), ":="( VALEUR = VALEUR %>% str_replace_all(",", ".")) 
  ][  NUM_LIGNE == ID_INDICE & str_detect( VALEUR, "[0-9]{3} [0-9]{3}")     , ":="( VALEUR = VALEUR %>% str_replace_all("\\s{2,}", "||") %>% str_replace_all(" ", "") %>% str_replace_all("\\|\\|", "  ") ) 
  ]

# df5[  NUM_LIGNE == ID_INDICE]
# df5[  NUM_LIGNE == ID_INDICE | str_detect(TEXT, req_date)]

# Correction espace au milieu d'une date
df5[, ":="(pos=0, long=0)]

nb_boucles = df5[str_detect(TEXT, req_date)]$TEXT %>% str_count("[0-1][0-9]/3 1/20[0-9]{2}") %>% max

if (nb_boucles >0){
  lapply( seq(nb_boucles), function (x){
    
        df5[  str_detect(TEXT, req_date) & str_detect(TEXT, "[0-1][0-9]/3 1/20[0-9]{2}") , ':='( pos  = TEXT %>% str_locate("[0-1][0-9]/3 1/20[0-9]{2}")%>% as.data.frame %>% .$start %>% `+`(3)
                                                                                               , long = TEXT %>% str_length )
          ][  pos > 0, ':='(  TEXT = paste0( str_sub( TEXT, 1, pos) , str_sub( TEXT, pos+1, long )))
          ]
        
        if ( any(names(df5) %in% c("pos", "long") ))
            df5[, c("pos", "long"):= NULL]
  }
  )
}

df5[, ':='(ID_DATE = 0, DATE_EXTRACT = NA_character_)]

df5[ str_detect(TEXT, req_date) &  NUM_LIGNE < ID_INDICE, ':='(ID_DATE = NUM_LIGNE)]
df5[, ':='(ID_DATE = max(ID_DATE, na.rm = TRUE)), by=.(MOT_RECHERCHE,ID_INDICE)]
df5[ ID_DATE == NUM_LIGNE, ":=" (DATE_EXTRACT = TEXT %>% str_trim %>% str_split("\\s{2,}") %>% 
  sapply( function(x) x %>% str_extract(req_date) %>% paste(collapse = "  ")) %>% str_replace('^(NA\\s{2,})+', "")
  ) ]
df5[, ":=" (DATE_EXTRACT = min(DATE_EXTRACT, na.rm = TRUE)), by =.(MOT_RECHERCHE,ID_INDICE) ]

df5[ID_DATE == NUM_LIGNE]

df5[ID_INDICE == NUM_LIGNE]

df6 = copy(df5)

df6[, ':='(ID_MONNAIE = NA_integer_, MONNAIE_EXTRACT = NA_character_)]

df6[str_detect(TEXT, req_monnaie), ":="(ID_MONNAIE = NUM_LIGNE ) ]
df6[, ':='(ID_MONNAIE = min(ID_MONNAIE, na.rm = TRUE)), by=.(MOT_RECHERCHE,ID_INDICE)
  ][ NUM_LIGNE == ID_MONNAIE, ":=" ( MONNAIE_EXTRACT = TEXT )
  ][, ":=" (MONNAIE_EXTRACT= min(MONNAIE_EXTRACT, na.rm = TRUE)), by =.(MOT_RECHERCHE,ID_INDICE) ]

df7 = copy(df6)

df7[, ':='(ID_UNITE = NA_integer_, UNITE_EXTRACT = NA_character_)]

df7[str_detect(TEXT, req_unite), ":="(ID_UNITE = NUM_LIGNE ) ]
df7[, ':='(ID_UNITE = min(ID_UNITE, na.rm = TRUE)), by=.(MOT_RECHERCHE,ID_INDICE)
  ][ NUM_LIGNE == ID_UNITE, ":=" ( UNITE_EXTRACT = TEXT )
  ][, ":=" (UNITE_EXTRACT= min(UNITE_EXTRACT, na.rm = TRUE)), by =.(MOT_RECHERCHE,ID_INDICE) ]
}
    
df7
# df7[ NUM_LIGNE == ID_INDICE & VALEUR != ""]
  }
,error=function(e) print(paste("error sur ", i)))
  # if(is(tt,"error")) { print(paste("error sur ", i))
  # }else{  tt
  #   
# }
 
} 
