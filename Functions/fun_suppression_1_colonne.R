fun_suppression_1_colonne = function( df_0 , MOT_EXTRAIT ){
  # df = copy(table1_0c)
  df = copy(df_0)
 
  if( nrow( df ) > 10){
    
    TEST_DEBUT_MOT = df$ TEXT %>% .[length(.)] %>% str_locate( fixed(MOT_EXTRAIT) ) %>% as.data.frame %>% .$start
    TEST_LONGEUR   = df$ TEXT  %>% nchar %>% max
    # TEST_2_COLONNES = (TEST_LONGEUR/2) < TEST_DEBUT_MOT
    TEST_2_COLONNES = ( TEST_DEBUT_MOT/TEST_LONGEUR) %>% `>` (0.20) %>% isTRUE()
    
    if( TEST_2_COLONNES ) str_sub(df$TEXT, 1, TEST_DEBUT_MOT -1) = ""
    
    # TTT : Suppression colonne droite =========
    BASE_SUP_COL_DROITE = df$TEXT %>% tail(1)
    indices_chiffres    = BASE_SUP_COL_DROITE %>% str_locate_all("[[:digit:],\\.]+") %>% as.data.table()
    indices_mot         = BASE_SUP_COL_DROITE %>% str_locate_all("[[:alpha:],\\.]+") %>% as.data.table()
    indices_espaces     = BASE_SUP_COL_DROITE %>% str_locate_all("\\s{2,}") %>% as.data.table()
    indices_tabulation  = BASE_SUP_COL_DROITE %>% str_detect("\t")
    
    indices_chiffres = indices_chiffres [start >  min(indices_mot$start), ]
    
    if (nrow(indices_chiffres)>0){
      indices_espaces = indices_espaces %>% .[ start > min(   indices_chiffres ),]
    }
    
    indices_espaces[,":="(lag_end = lag(end) )]
    indices_espaces[, ecart := end-start]
    indices_espaces  = indices_espaces[ecart>80]
    
    if ( nrow( indices_espaces) > 0 & !indices_tabulation ){
      BASE_SUP_COL_DROITE = str_sub(BASE_SUP_COL_DROITE, 1, min(indices_espaces$lag_end))
    }
    
    max_longueur = df$TEXT %>% nchar %>% max
    indice_num       = BASE_SUP_COL_DROITE %>% str_locate_all("[:digit:]") %>% unlist %>% .[!is.na(.)]
    indice_character = BASE_SUP_COL_DROITE %>% str_locate_all("[:alpha:]+") %>% as.data.table() %>% .$start %>% .[!is.na(.)]
    
    if( length(indice_num )>0 & length(indice_character)>0){
      
          if( min(indice_num)< min(indice_character) ){
            indice_num  =  indice_num %>% .[. > min(indice_character) ]
          }
          if ( length(indice_num ) == 0 ) indice_num = 0
          indice_character = indice_character %>% .[ . > min(indice_num) ] %>% `-`(1)
          
          dernier_indice = max(indice_num, 0, na.rm=TRUE)
          
          if (length(indice_character)>0){
            dernier_indice = min( dernier_indice, indice_character )
          }
          
          # table1_0d = copy(df)
          # RA 81 p 67 : traitement applicatable si très peu de tabulation
          if ( df$TEXT %>% str_count("\t") %>% sum <20 ){
              if( (dernier_indice / max_longueur) < 0.7){
                df[, TEXT := str_sub( TEXT, 1, dernier_indice)]
              }
          }
      }
      df = df[TEXT != "",]
  }
  df
}