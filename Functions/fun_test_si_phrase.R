fun_test_si_phrase = function( chaine ){
# fun_test_si_phrase = function( chaine , premier_test = TRUE){
  # chaine = df_t[ NUM_LIGNE == x]$TEXT %>% str_replace( MOT, "" )
  # chaine =  "WERE UP BY 63.3% AND      MONEY MARKET LOANS                               139,005         152,000"
  # chaine = "90.2        90.8"
  # chaine = "28,037 26,834"
  # chaine = "     (1) NET INTEREST INCOME                                   20,626,208.67       14,879,047.01  20,852,997.14      15,050,187.55"
  # Test si ligne du milieu est vide
  # Test vide
  # TEST_VIDE = FALSE
  base_chaine = chaine %>% str_replace("^\\s+", "")%>% str_split("\\s{2,}")
  
  # if(premier_test){
  #   # TEST_VIDE = str_count(chaine, "[[:alpha:]%\\(\\)]") == 0
  #   TEST_VIDE = (nchar(chaine) == 0)| chaine == "  "
  # }
  
  # Test si au moins un double espace
  TEST_SEP = chaine %>% str_count("\\s{2,}") %>% max(0) %>% `>`(0)
  
  if( !TEST_SEP ){
    TEST_SEP = isTRUE( str_detect(chaine, "\\s") & str_detect(chaine, "[:digit:]") & !str_detect(chaine, "[:alpha:]"))
  }
  
  # locate_parenthese = base_chaine %>% lapply( function(x){ x %>% str_locate_all('([0-9]+)') %>% lapply(as.data.frame) })
  # if ()
  
  # Test si au moins 3 chiffres consécutifs
  TEST_CHIFFRE =  base_chaine  %>%
                  lapply( function(x) x[ str_detect(x,"[:alnum:]")]) %>%
                  # lapply( function(x) ( str_count(x, "[[:alpha:]%\\(\\)]") < 2 ) & 
                  lapply( function(x) ( str_count(x, "[[:alpha:]%]") < 2 ) & 
                                      ( str_count(x, "[:digit:]{2,}|[:digit:].[:digit:]"     ) > 0 ) &
                                      ( str_count(x, "[:digit:]-[:digit:]") == 0 ) &
                                      ( str_count(x, "[:digit:]") > 2 )
                  )%>%
                  unlist() %>%
                  any()
  
  TEST_LETTRE = FALSE
  
  if( length(base_chaine) > 0){
    # TEST_LETTRE = base_chaine %>% .[[1]] %>% .[1] %>% str_count("[[:alpha:]]") > 0
    TEST_LETTRE = base_chaine %>% sapply(function(x) x[1] %>% str_count("[[:alpha:]]") == 0 ) %>% any
   }
  
  any( TEST_SEP & TEST_CHIFFRE & TEST_LETTRE ) #& TEST_SI_NO_PHRASE)
  # any( TEST_VIDE | ( TEST_SEP & TEST_CHIFFRE & TEST_LETTRE )) #& TEST_SI_NO_PHRASE)
}



# OLD VERSION
# fun_test_si_phrase = function( lignes_a_tester ){
#   
#   # BASE_TEST_TABLEAU = document %>%
#   #                    .[ -1:1 + x ] %>%
#   #                    .[ NUM_PAGE == document[x]$NUM_PAGE ,] %>% 
#   #                    .$TEXT
# 
#   # Test si au moins un double espace
#   TEST_SEP_DOUBLE_ESPACE = lignes_a_tester %>% 
#                            str_count("\\s{2,}") %>% 
#                            max %>% 
#                            `>`(0)
#   
#   # Test si au moins un chiffre entouré d'un double espace
#   TEST_AU_MOINS_UN_CHIFFRE =  lignes_a_tester %>%
#                               str_split("\\s{2,}") %>%
#                               lapply( function(x) x[ str_detect(x,"[:alnum:]")]) %>%
#                               lapply( function(x) (str_count(x, "[[:alpha:]%\\(\\)]") < 2) & str_count(x, "[:digit:]{3,}")>0 ) %>%
#                               # lapply( function(x)( x < 2 ) ) %>%
#                               unlist() %>%
#                               any()
#   
#   # Ajout test RA 17 p129 une ligne TA hors tableau
#   TEST_SI_NO_PHRASE = !(  str_detect( lignes_a_tester[2], "[:digit:]") 
#                         & str_detect( lignes_a_tester[2], paste0( LISTE_MONNAIE, collapse = "|"))
#                         & str_detect( lignes_a_tester[2], paste0( LISTE_UNITE  , collapse = "|")) ) & 
#                       !str_detect( lignes_a_tester[2], "RATIO|RATES AND FINANCIAL ASSETS") & 
#                       !str_detect( lignes_a_tester[2], paste(MOT, "WERE UP BY")) 
#                        # ( str_count( lignes_a_tester, "[:digit:]") < 3 & str_count( lignes_a_tester,"[:alpha:]")
#   
#   
#   TEST_SEP_DOUBLE_ESPACE & TEST_AU_MOINS_UN_CHIFFRE & TEST_SI_NO_PHRASE
# }