fun_ajout_sep_num_3_1_3 = function(df){
  
    table3 = df
    
    TEST_SEP_2_CHIFFRES = table3 %>% str_locate_all( "[0-9]{3} [0-9] [0-9]{3}")
    
    x5 =  TEST_SEP_2_CHIFFRES %>% 
          seq %>%
          lapply( function(x) as.data.table(TEST_SEP_2_CHIFFRES[x])[, NUM := x ] ) %>% rbindlist
    
    # table3= copy(table3)
    if(nrow(x5)>0){
      for (i in seq(nrow(x5))){
        str_sub( table3[x5$NUM[i]], x5$start[i]+3, x5$start[i]+3) <- "|"
      }
    }
    
    table3
  
}