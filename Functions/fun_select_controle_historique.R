fun_select_controle_historique = function( df , type = "ASSET", df2 = NULL, LISTE_TOTAL_MOTSb = LISTE_TOTAL_MOTS_0){
      # df = corpus_raw3[!is.na(MONTANT_IN_MILLION_USD)]; type = LISTE_TOTAL_MOTS$ASSET; df2 = NULL
      #  df = corpus_raw3[!is.na(MONTANT_IN_MILLION_USD)]; type = LISTE_TOTAL_MOTS$EQUITY; df2 = PERIMETRE_ASSET 
      #  df = PERIMETRE_0; type = "EQUITY" ; df2 =toto_ASSET 
      # Selection du perimetre sur le mot recherche
      PERIMETRE = df [ TYPE %in% type & !is.na(DEVISE_CONSO) & !is.na(MNT) 
                      
                    ][ , ANNEE_CONSO := year(DATE_CONSO)
                       
                    ][                  , TOP_DEVISE := (DEVISE == DEVISE_CONSO)*1 
                    ][ is.na(TOP_DEVISE), TOP_DEVISE := 0
                    ][                  , ":="( TOP_ASSETS = 0, ORDRE = 9999999999999)  ]
      
      # Patch pour NAVBARB et asset
      if ( type == "ASSET"){
          #PERIMETRE = PERIMETRE[ !(TYPE %in% "ASSET" & ACRONYM == "NABARD" & COUNTRY == "India" & !str_detect(MOT_EXTRAIT,"USES OF FUNDS"))]
          PERIMETRE = PERIMETRE[ !(TYPE %in% "ASSET" & CODE_BANK == "PT20141" & str_detect(MOT_EXTRAIT,"TOTAL ASSETS"))]
      }
      if (type == "INCOME"){
          PERIMETRE[TYPE %in% "INCOME" & CODE_BANK == "BG19991" & nchar(as.integer(MNT)) %in% c(4,5,6) & MNT >0 & is.na(UNITE) 
            , ":=" (UNITE ="THOUSANDS"
              , MNT = MNT*1000
              , MONTANT_USD =  MONTANT_USD*1000
              , MONTANT_FINAL = MONTANT_FINAL*1000
              , MONTANT_IN_MILLION_USD = MONTANT_IN_MILLION_USD*1000 )]
      }
      
      if (!is.null(df2)){
        
          PERIMETRE =  PERIMETRE %>% 
                       merge(   df2[,.( NOM_FICHIER, NUM_PAGE, ANNEE_CONSO , TOP_ASSETS_NEW = 1) ]
                              , by = c("NOM_FICHIER", "NUM_PAGE", "ANNEE_CONSO")
                              , all.x = TRUE )
          
          PERIMETRE[ TOP_ASSETS_NEW == 1 , TOP_ASSETS := 1 ]
          
      }
      
      for(i in 1: length(LISTE_TOTAL_MOTSb[[type]])){
        PERIMETRE[ str_detect(MOT_EXTRAIT, LISTE_TOTAL_MOTSb[[type]][i]) & ORDRE == 9999999999999 , ':='(ORDRE = i)]
        # PERIMETRE_VALIDE[CODE_BANK == CODE_BANK_U & TYPE=="ASSET" & ANNEE == 2018]$MOT_EXTRAIT][ str_detect(MOT_EXTRAIT, LISTE_TOTAL_MOTSb[[type]][i]), LISTE_TOTAL_MOTSb[[type]][i]) %>% print()
      }
      
      # Ajout ordre bank compagny
      PERIMETRE[,":="(ORDRE_BANK = 0)][ str_detect(BANK_GROUP,"CONSOLIDATED|GROUP"),":="(ORDRE_BANK = 1)]

      setorder( PERIMETRE, CODE_BANK, -DATE_CONSO, -ANNEE_RA, ORDRE, -ORDRE_BANK , -MONTANT_USD, -TOP_ASSETS, -TOP_DEVISE, NUM_LIGNE )
      
      PERIMETRE_DEDOUB = PERIMETRE %>% unique( by = c( "CODE_BANK",  "DATE_CONSO", "ANNEE_RA",  "ORDRE", "ORDRE_BANK", "MONTANT_USD") )
      
      setorder( PERIMETRE_DEDOUB , CODE_BANK,-TOP_ASSETS, -TOP_DEVISE, -DATE_CONSO, ORDRE, -ORDRE_BANK,-MONTANT_USD, -ANNEE_RA, NUM_LIGNE )
    
      LISTE_RA = PERIMETRE_DEDOUB $ANNEE_RA %>% unique %>% sort %>% rev %>% .[-1]
      
      PERIMETRE_A_VALIDE = PERIMETRE_DEDOUB[ , .( CODE_BANK, DATE_CONSO, ANNEE_RA, MONTANT_USD, TOP_CHIFFRE_VALIDE = 0 )]
      
      for ( x in LISTE_RA ) {
        
        PERIMETRE_A_VALIDE = PERIMETRE_A_VALIDE[ , .( CODE_BANK, DATE_CONSO, ANNEE_RA, ID = 1, MONTANT_USD, TOP_CHIFFRE_VALIDE )
                    ][ ANNEE_RA == x, ID := 0 ] %>%
          
                merge( PERIMETRE_DEDOUB[ANNEE_RA == x, .( CODE_BANK, DATE_CONSO, ID = 1, MONTANT_USD_2 = MONTANT_USD )]
                       , by = c( "CODE_BANK", "DATE_CONSO", "ID" )
                       , allow.cartesian = TRUE
                       , all.x = TRUE ) %>%
          
          .[, ":="( ECART = abs( (MONTANT_USD_2 - MONTANT_USD) / MONTANT_USD)) ] %>% 
          .[ is.na(ECART), ECART := 0 ] %>%
          .[, ":="(ECART_MIN = min(ECART, na.rm = TRUE)), .(CODE_BANK, DATE_CONSO, ID, ANNEE_RA, MONTANT_USD )] %>% 
          .[ ECART_MIN == ECART, ] %>%
          .[ !is.na(MONTANT_USD_2) , ":=" ( TOP_CHIFFRE_VALIDE = TOP_CHIFFRE_VALIDE + (ECART<0.1) )  ] %>%
          .[, ":="( MONTANT_USD_2 = NULL, ECART = NULL, ECART_MIN = NULL, ID = NULL )] 
        
      }

      PERIMETRE_A_VALIDE[, ":="(VALIDE_AU_MOINS_1 = (TOP_CHIFFRE_VALIDE>0)*1)]
      
      PERIMETRE_VALIDE = PERIMETRE_DEDOUB %>% 
                         merge(  PERIMETRE_A_VALIDE, by = c("CODE_BANK", "DATE_CONSO", "ANNEE_RA", "MONTANT_USD"))
      
      # PERIMETRE_VALIDE[, ":="(MEDIAN_MNT = median(MNT) ), by = .(CODE_BANK)]
      # PERIMETRE_VALIDE[, ":="(ECART_MEDIAN = MNT /MEDIAN_MNT)]
      
      # toto3b= toto3b[ ECART_MEDIAN <300,]
      
      setorder( PERIMETRE_VALIDE, CODE_BANK, -ANNEE_CONSO, -TOP_ASSETS, -DATE_CONSO, -VALIDE_AU_MOINS_1, -TOP_DEVISE, ORDRE,-MONTANT_USD, -ANNEE_RA )
      
      # PERIMETRE_VALIDE_DEDOUB2 = PERIMETRE_VALIDE[, ":="(N = 1:.N), by = .(CODE_BANK, ANNEE_CONSO)][N==1]
      PERIMETRE_VALIDE_DEDOUB = PERIMETRE_VALIDE %>% unique( by = c("CODE_BANK", "ANNEE_CONSO") )
      
      if(type %in% c("ASSET", "EQUITY")){
            
          PERIMETRE_VALIDE_DEDOUB[, ":="(MEDIAN_MNT = median(MONTANT_USD) ), by = .(CODE_BANK)]
          PERIMETRE_VALIDE_DEDOUB[, ":="(ECART_MEDIAN = abs(MONTANT_USD /MEDIAN_MNT))]
          
          # Si ugros écart et moins de 3 chiffres
          PERIMETRE_VALIDE_DEDOUB[ , ":="(  MAX_ECART = max(ECART_MEDIAN,0,na.rm=TRUE)
                                          , NB_RA = .N), by = .(CODE_BANK)]
          
          PERIMETRE_VALIDE_DEDOUB = PERIMETRE_VALIDE_DEDOUB[!(ECART_MEDIAN > 100 & NB_RA < 4)] 
          PERIMETRE_VALIDE_DEDOUB = PERIMETRE_VALIDE_DEDOUB[!(ECART_MEDIAN > 1750)]
          PERIMETRE_VALIDE_DEDOUB = PERIMETRE_VALIDE_DEDOUB[!(ECART_MEDIAN > 0 & ECART_MEDIAN < 0.2)]
          
          
          PERIMETRE_VALIDE_DEDOUB[, ":="(MEDIAN_MNT = median(MONTANT_USD) ), by = .(CODE_BANK)]
          PERIMETRE_VALIDE_DEDOUB[, ":="(ECART_MEDIAN = abs(MONTANT_USD /MEDIAN_MNT))]
        
          # Si ugros écart et moins de 3 chiffres
          PERIMETRE_VALIDE_DEDOUB[ , ":="(  MAX_ECART = max(ECART_MEDIAN,0,na.rm=TRUE)
                                            , NB_RA = .N), by = .(CODE_BANK)]
          
          PERIMETRE_VALIDE_DEDOUB = PERIMETRE_VALIDE_DEDOUB[!(ECART_MEDIAN > 28)]
          PERIMETRE_VALIDE_DEDOUB = PERIMETRE_VALIDE_DEDOUB[!(ECART_MEDIAN > 0 & ECART_MEDIAN < 0.2)]
      }
      # PERIMETRE_VALIDE_DEDOUB[, ":="(MEDIAN_MNT = median(MNT) ), by = .(CODE_BANK)]
      # PERIMETRE_VALIDE_DEDOUB[, ":="(ECART_MEDIAN = MNT /MEDIAN_MNT)]
      # PERIMETRE_VALIDE_DEDOUB = PERIMETRE_VALIDE_DEDOUB[!(ECART_MEDIAN > 0 & ECART_MEDIAN < 0.006)]
      # PERIMETRE_VALIDE_DEDOUB = PERIMETRE_VALIDE_DEDOUB[!(ECART_MEDIAN > 8)]
      # # Traitement AFD on enlève proparco
      # CODE_BANK FR19411 et FR19771 respectifs
      # toto3d[ CODE_BANK== "FR19411",.(ANNEE_CONSO, MNT)]
      
      PERIMETRE_VALIDE_AFD =  PERIMETRE_VALIDE_DEDOUB %>% 
                              merge( PERIMETRE_VALIDE_DEDOUB[ CODE_BANK =="FR19771"
                                                              , .( CODE_BANK ="FR19411", ANNEE_CONSO , MNT_PROPARCO = MNT )]
                                   , by = c("CODE_BANK", "ANNEE_CONSO")
                                   , all.x = TRUE )
      
      # Contrôle suppression des chiffres AFD si PROPARCO non présent sur certaine année
      PERIMETRE_VALIDE_AFD = PERIMETRE_VALIDE_AFD[ !(CODE_BANK =="FR19411" & is.na(MNT_PROPARCO))]
      
      # Mise à jour des montants 
      PERIMETRE_VALIDE_AFD[ !is.na(MNT_PROPARCO), MNT := MNT - MNT_PROPARCO]
      
      # Consolidation Banque si unité manquante
      # PERIMETRE_VALIDE_AFD[ nchar(MNT) < 8] 
      
      # VN20061
      
      PERIMETRE_VALIDE_AFD [ !is.na(MNT_PROPARCO) ,":="(  MONTANT_USD            = MNT/UNITES_PAR_USD )
                          ][ !is.na(MNT_PROPARCO) ,":="(  MONTANT_FINAL          = MONTANT_USD /1000000000
                                                        , MONTANT_IN_MILLION_USD = MONTANT_USD /1000000    )
                          ]
      
      PERIMETRE_VALIDE_AFD
  
}

