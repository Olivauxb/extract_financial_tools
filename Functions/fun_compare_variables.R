#'  Permet de comparer deux dataframes (ou datatables) variable par variable
#'
#' @title Comparaison de 2 dataframe
#' 
#' @description La fonction indique, pour chaque variable, les données en écart.
#' @details  Les données qui n'apparaissent que dans un seul des deux dataframes sont également fournies en sortie
#' 
#' @param nom_table_old  nom du premier dataframe/datatable
#' @param nom_table_new  nom du deuxième dataframe/datatable
#' @param by     liste des variables permettant de faire la jointure entre les deux dataframes. 
#' @param ignore_NA_char TRUE si une valeur manquante est équivalente à une valeur vide (FALSE par défaut).
#' @param critere        ecart minimum accepté entre valeurs des variables numériques
#' 
#' @return une liste de dataframe. 
#' Chaque élément de la liste comprend un dataframe restreint aux variables de jointure et à la variable observant des écarts de données, 
#' un message de synthèse s'affiche dans la console indiquant le nombre de variables comparées, 
#' le nombre d'observations et la liste des variables en écart avec le nombre d'observations associé.
#' 
#' @export
#'
#' @examples
#' TABLE_ORIGINE=data.frame(ID_UNIQUE=c(1,2,3), VAR1=c(0,0,0))
#' TABLE_FINALE=data.frame(ID_UNIQUE=c(1,2,3,4), VAR1=c(0,1,0,0), VAR2=c(1,1,1,1))
#'                         
#' Compar_Variables(nom_table_old = "TABLE_ORIGINE",  nom_table_new = "TABLE_FINALE",   by = c("ID_UNIQUE"))
#'
fun_compare_variables = function(nom_table_old, 
                            nom_table_new, 
                            by, 
                            ignore_NA_char = FALSE,
                            critere = 0.00001) {
  
  if(!exists(nom_table_old) | !exists(nom_table_new)){
    warning("Les tables n'existent pas dans l'environnement")
    
  }else{
    
    # Vérification avant jointure ---------------------------------------------
    
    # Vérification : est ce que la clé de jointure est unique dans chacune des tables?
    dupold = sum( duplicated( eval( parse( text = nom_table_old))[,..by]))
    dupR   = sum( duplicated( eval( parse( text = nom_table_new))[,..by]))
    
    if(dupold > 0){
      warning(paste0("Les variables ",by," ne constituent pas une clé de jointure unique dans la table ",nom_table_old))
    }
    
    if(dupR > 0){
      warning(paste0("Les variables ",by," ne constituent pas une clé de jointure unique dans la table ",nom_table_new))
    }
  
    if(dupold == 0 & dupR == 0){
      
      # Jointure ----------------------------------------------------------------
      # Fusion des deux tables
#       COMPAR = merge(as.data.table(eval(parse(text = nom_table_old)))[, REF := 'old'],
#                      as.data.table(eval(parse(text = nom_table_new)))[, REF := 'new'],
#                      by = by, 
#                      suffixes = c(".old",".new"),
#                      all = TRUE)

      dataold = eval( parse( text = ifelse(  eval( parse( text = gsub("\\{x\\}", nom_table_old , "!( 'data.table' %in% class( {x} ) )"))) 
                                             , paste0("data.table(", nom_table_old, ")")
                                             , paste0("copy("      , nom_table_old, ")") )))
      
      dataR = eval( parse( text = ifelse(  eval( parse( text = gsub("\\{x\\}", nom_table_new , "!( 'data.table' %in% class( {x} ) )"))) 
                                           , paste0("data.table(", nom_table_new, ")")
                                           , paste0("copy("      , nom_table_new, ")") )))
      
      COMPAR = merge(dataold[, REF := 'old'],
                     dataR[, REF := 'new'],
                     by = by, 
                     suffixes = c(".old",".new"),
                     all = TRUE)
      
      # Noms des colonnes qui ne sont pas communes aux deux tables : 
      Col_noncommunes = names(COMPAR)[gsub("\\.old|\\.new", "", names(COMPAR)) %in% names(COMPAR)
                                      &  !names(COMPAR) %in% by]
      
     
      # Variables non communes aux deux tables ----------------------------------
      
      # Si des variables ne sont pas communes aux deux tables, l'utilisateur en est informé
      if(length(Col_noncommunes) >0){
        message(paste0(rep("-",90),collapse = ""))
        message('Variables présentes dans la table old mais pas dans la table new :')
        message(paste0(rep("-",90),collapse = ""))
        print(Col_noncommunes[Col_noncommunes %in% names(eval(parse(text = nom_table_old)))])
        message(paste0(rep("-",90),collapse = ""))
        
        message(paste0(rep("-",90),collapse = ""))
        message('Variables présentes dans la table new mais pas dans la table old :')
        message(paste0(rep("-",90),collapse = ""))
        print(Col_noncommunes[Col_noncommunes %in% names(eval(parse(text = nom_table_new)))])
        message(paste0(rep("-",90),collapse = ""))
      }
  
      # Observations non communes aux deux tables -------------------------------
      # Si des observations ne sont pas communes aux deux tables, l'utilisateur en est informé
      if(sum(is.na(COMPAR$REF.new)) > 0){
        print(paste0("Il y a ", sum(is.na(COMPAR$REF.new))," observations dans la table old qui ne sont pas dans la table new"))
      }
      
      if(sum(is.na(COMPAR$REF.old)) > 0){
        print(paste0("Il y a ", sum(is.na(COMPAR$REF.old))," observations dans la table new qui ne sont pas dans la table old"))
      }
      
      
      # On restreint COMPAR aux données partagées par les deux jeux de données
      COMMUN = COMPAR[ !is.na(COMPAR$REF.new) & !is.na(COMPAR$REF.old) ]
      
      # Noms de variables communes
      liste_noms =  names(COMMUN)[!names(COMMUN) %in% Col_noncommunes]
      
      # Nombre d'observations communes
      nb_obs = dim(COMMUN)[1]
      
      # Initialisation des variables 
      #j = 0
      liste_ecarts = list()
      liste_type = c()
      lst_obs_ecart = c()
      
      # Liste des variables hors celles permettant la fusion  
      liste = unique(gsub("\\.old|\\.new", "",liste_noms[!liste_noms %in% c(by,"REF.old","REF.new")]))
      liste_old = paste0(liste,".old")
      liste_R = paste0(liste,".new")
      
      # Pour chacune des variables, on compare les données issues de chacune des tables

      # Boucle ------------------------------------------------------------------
      for(i in 1:length(liste)){
        
        # Initialisation du changement de type
        change_type = 0
        
        # Comparaison de la variable de la table old et de la variable de la table new 
        var_old = COMMUN[,liste_old[i], with=FALSE]
        eval(parse(text = paste0("typeold = class(var_old[,", liste_old[i],"])")))
        
        var_r = COMMUN[,liste_R[i], with=FALSE]
        eval(parse(text = paste0("typeR = class(var_r[,", liste_R[i],"])")))
        
        # Type des variables ------------------------------------------------------
                
        # Si une des variables est de type numeric et l'autre est de type int, on convertit en numeric
        if(typeold[1] == "integer" & typeR[1] == "numeric"){
          
          obj = paste0("var_old = var_old[,.(",liste_old[i],"=as.numeric(",liste_old[i],"))]")          
          eval(parse(text = obj))
          
          change_type = 1
          
        }else{
          if(typeold[1] == "numeric" & typeR[1] == "integer"){

            obj = paste0("var_r = var_r[,.(",liste_R[i],"=as.numeric(",liste_R[i],"))]")          
            eval(parse(text = obj))
                
            change_type = 1
          }else{
           if(typeold[1] == "character" & typeR[1] == "character" & ignore_NA_char){
             obj = paste0("var_r = var_r[,.({VAR} = replace({VAR}, is.na({VAR}), ''))]")          
             eval(parse(text = gsub("\\{VAR\\}",liste_R[i],obj)))
             
             obj = paste0("var_old = var_old[,.({VAR} = replace({VAR}, is.na({VAR}), ''))]")          
             eval(parse(text = gsub("\\{VAR\\}",liste_old[i],obj)))
             
           } 
          }
        }
          
        
        # Observations en écart ---------------------------------------------------
        
        # Si les variables ne sont pas du même type
        if (sum(typeold == typeR) == 0 & change_type == 0){
          
#           j = j+1
          liste_type = c(liste_type, paste0("La variable ",liste[i]," n'est pas du même type dans les deux tables, la comparaison n'a donc pu être effectuée"))
              
          nbobsecart = 0  
          
          # Si les variables sont du même type on observe les écarts
          }else{
            
            if(typeold[1]  == "numeric"){
              
              # Ecart entre les variables numeriques (4 chiffres après la virgule)
              verif_ecart = (((abs(var_old-var_r) < critere)
                              & (!is.na(var_old) & !is.na(var_r)))
                             | (is.na(var_old) & is.na(var_r)))
              
              }else{
                
                
                
                # Ecart entre les variables de type character/booleen etc.
                verif_ecart = ((var_old == var_r
                                & (!is.na(var_old) & !is.na(var_r)))
                               | (is.na(var_old) & is.na(var_r)))
              }
            
            # Nombre d'observations en écart pour cette variable
            nbobsecart = nb_obs - sum(verif_ecart, na.rm = TRUE)
          }
              
        # Si nbobsecart est différent de 0 alors on récupère les observations qui ne sont pas communes
        
        if(!nbobsecart == 0){      
          ECART = COMMUN[which(verif_ecart == FALSE), c(liste_old[i], liste_R[i],by), with = FALSE]  
          liste_ecarts[[liste[i]]] = ECART
          lst_obs_ecart = c(lst_obs_ecart, nrow(ECART))
        }
      } # Fin de boucle
      
      # Observations non communes aux deux tables -------------------------------
 
      # Observations dans la table old mais pas dans la table new
      if(sum(is.na(COMPAR$REF.new)) > 0){
        liste_ecarts[["Observations old uniquement"]] = COMPAR[ is.na(REF.new)][,c(liste_old, by), with=FALSE] 
        lst_obs_ecart = c(lst_obs_ecart, nrow(liste_ecarts[["Observations old uniquement"]]))
      }
      
      # Observations dans la table new mais pas dans la table old
      if(sum(is.na(COMPAR$REF.old)) > 0){
        liste_ecarts[["Observations new uniquement"]] = COMPAR[ is.na(REF.old)][,c(liste_R,by), with=FALSE] 
        lst_obs_ecart = c(lst_obs_ecart, nrow(liste_ecarts[["Observations new uniquement"]]))
      }

      
      # Synthèse ----------------------------------------------------------------
      # Nombre d'observations en écart
      # Nombre / liste des variables en écart
      # Variables qui ne sont pas du même type

      message(paste0(rep("-",90),collapse = ""))
      message(paste0(rep(" ",20),collapse = ""),"SYNTHESE DE LA COMPARAISON DES TABLES")
      message(paste0(rep("-",90),collapse = ""))

      message(paste0("Nombre d'observations comparées : ",nb_obs))
      message(paste0("Nombre de variables comparées   : ",length(liste)))

    
      if(length(liste_ecarts) > 0){
        message(paste0("Il y a ",length(names(liste_ecarts)),
                       " variables pour lesquelles des différences ont été observées"))
        
        message(cat(c("Les écarts entre observations concernent les variables suivantes :",
                      paste0(1:length(names(liste_ecarts)), ") ", 
                             names(liste_ecarts), " : ",lst_obs_ecart," observations différentes")),sep="\n"))
        
      }else{
       
        # Variables pour l'affichage
        ch1 = " .---. "
        ch2 = "/ ^ ^ \\"
        ch3 = "\\ \\_/ /"
        ch4 = " `---´ "
        msg = "Il n'y a pas de différence entre les variables communes aux deux tables"
        blank     = paste0( rep(" ",75), collapse = "")
        miniblank = paste0( rep(" ",29), collapse = "") 
        
        # Affichage du message
        message(paste0(rep("-",90),collapse = ""))
        cat(paste(ch1,blank,ch1), 
                    paste(ch2,miniblank,"!FELICITATIONS!",miniblank,ch2),
                    paste(ch3," ",msg," ",ch3),
                    paste(ch4,blank,ch4)
                    ,sep="\n")
        message(paste0(rep("-",90),collapse = ""))
        
        #message("Il n'y a pas de différence entre les variables communes aux deux tables")
        
      }
      
      if(!is.null(liste_type)){
        cat(liste_type,sep="\n")
      }
      
      # Liste des ecarts
      return(liste_ecarts)
    }
  }
}





