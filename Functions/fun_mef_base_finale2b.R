# LISTE_PERIMETRE = LISTE_TT_ASSET; NOM_PERIMETRE = "ASSETS"
# LISTE_PERIMETRE = LISTE_NET ;  NOM_PERIMETRE = "NET_INCOME"
fun_mef_base_finale2b = function(base = corpus_raw3, MOT = "ASSET", EXPORT = FALSE, DB = DATA_BASE_PDB, ANNEE_REF = 2018, ANNEE_MIN = 2008){
    # base  = readRDS("corpus_raw_tt_20210113.RDS"); "ASSET" , TRUE
    # base = corpus_raw3; MOT = "ASSET" ; EXPORT = FALSE; DB = DATA_BASE_PDB;ANNEE_REF = 2018
    # base = corpus_raw3; MOT = "EQUITY"; EXPORT = FALSE; DB = DATA_BASE_PDB
    # base = corpus_raw3; MOT = "NETINCOME"; EXPORT = FALSE; DB = DATA_BASE_PDB; ANNEE_REF = 2018; ANNEE_MIN = 2008
    # base = corpus_raw3; MOT = "NETPROFIT"; EXPORT = FALSE; DB = DATA_BASE_PDB; ANNEE_REF = 2018; ANNEE_MIN = 2008
    # ID_NEW = DB[,.(CODE_BANK)]
    DBP = read_xlsx("Test Commercial vs manually 2018.xlsx") %>% setDT
    PERIMETRE_0 = base[!is.na(MONTANT_IN_MILLION_USD) & CODE_BANK != "FR18161"]
    PERIMETRE_0 = PERIMETRE_0 [MONTANT_IN_MILLION_USD < 10000000]
    base_assets = NULL
    
    if (MOT == "EQUITY") base_assets = fun_select_controle_historique(PERIMETRE_0, "ASSET")
    
    toto2 = fun_select_controle_historique( PERIMETRE_0, MOT , base_assets) 
    
    DATA_BASE_PDB[,.(Nb,CODE_BANK)] %>%
      merge(  toto2 , all.x = TRUE, by = "CODE_BANK") %>%
      dcast(Nb +CODE_BANK ~ ANNEE_CONSO, value.var = "MONTANT_IN_MILLION_USD") %>%
      write_xlsx('TEST_USD.xlsx')
    
    
    toto2 %>%
      merge( DATA_BASE_PDB[,.(Nb, CODE_BANK )], all.y = TRUE, by = "CODE_BANK") %>%
      dcast(Nb +CODE_BANK ~ ANNEE_CONSO, value.var = "MNT") %>%
      write_xlsx('TEST_ML.xlsx')
    
    if (EXPORT & nrow(toto2)>0 ){
        write_xlsx(  toto2 %>% 
                     dcast(CODE_BANK ~ ANNEE_CONSO, value.var = "MONTANT_IN_MILLION_USD")
                   , file.path(config$output, gsub("-", "_", paste0( MOT, "_", Sys.Date(), ".xlsx") ) ) )
    }
    
    toto2[, ":="(   NOM_COLONNE_1 = paste0( MOT, "_AUTO_DEVISE" , ANNEE )
                  , NOM_COLONNE_2 = paste0( "DEVISE_", ANNEE )
                  , NOM_COLONNE_3 = paste0( "TAUX_DEVISE", ANNEE )
                  , NOM_COLONNE_4 = paste0( MOT,"_MLS_USD_", ANNEE ) ) ]
    DB2 = copy(DB)
    setnames(DB2, names(DB2), DB2 %>% names %>% toupper %>% str_replace_all('\\.+', "."))
    
    select_variables = names(DB2) %>% str_subset( paste0("CODE_BANK|", str_replace( MOT,"NET", ""), ".?.MLS.USD"))
    select_variables
    DB2[, ..select_variables]
    
    BASE_FINALE = DB2[!is.na(CODE_BANK), ..select_variables] 
    
    if (nrow(toto2)>0) {
        BASE_FINALE  = BASE_FINALE %>%
        # merge( ID_NEW , by = c("CODE_BANK", "Bank", "Country"), all = TRUE) %>%
        # merge( dcast( toto2,  CODE_BANK  ~ NOM_COLONNE_1 , value.var = "MNT"           ), by = "CODE_BANK", all = TRUE ) %>%
        # merge( dcast( toto2,  CODE_BANK  ~ NOM_COLONNE_2 , value.var = "DEVISE_CONSO"  ), by = "CODE_BANK", all = TRUE ) %>%
        # merge( dcast( toto2,  CODE_BANK  ~ NOM_COLONNE_3 , value.var = "UNITES_PAR_USD"), by = "CODE_BANK", all = TRUE ) %>%
        merge( dcast( toto2,  CODE_BANK  ~ NOM_COLONNE_4 , value.var = "MONTANT_IN_MILLION_USD" ), by = "CODE_BANK", all = TRUE ) 
    
        colonnes_order = lapply( toto2$NOM_COLONNE_1 %>% unique %>% length %>% seq
                                 , function(i){
                                   list(  
                                     # toto2$NOM_COLONNE_1 %>% unique %>% sort
                                     # , toto2$NOM_COLONNE_2 %>% unique %>% sort
                                     # , toto2$NOM_COLONNE_3 %>% unique %>% sort
                                     # , 
                                     toto2$NOM_COLONNE_4 %>% unique %>% sort ) %>%
                                     sapply( function(x) x[i] )
                                 }
        ) %>% unlist
        
        setcolorder( BASE_FINALE, c(select_variables, colonnes_order ))
        
    }  
   
    setorder( BASE_FINALE, CODE_BANK, na.last= TRUE)
    
    # Conso chiffres
    var_a_consolider = names(BASE_FINALE) %>% str_subset(as.character(ANNEE_REF))
    
    if ( length( var_a_consolider ) > 0 ){
        BASE_FINALE = "BASE_FINALE[!is.na({var}), ':='({var_a_consolider} = {var})]" %>% 
                      str_glue( var = select_variables[2]) %>%
                      parse(text=.) %>%
                      eval()
        
        BASE_FINALE = BASE_FINALE[,-..select_variables[2]]
        
    }else{
      
        setnames( BASE_FINALE, select_variables[2], paste0( MOT,"_MLS_USD_", ANNEE_REF ) )
        
        la = lapply( ANNEE_MIN : year(Sys.Date()) %>% .[!. %in% ANNEE_REF]
              , function(x){
                BASE_FINALE[, VAR_INTER :=.(NA_real_) ]
                setnames( BASE_FINALE, "VAR_INTER", paste0( MOT,"_MLS_USD_", x ))
              })
        
        colonnes_order = BASE_FINALE %>% names %>% .[-1] %>% unique %>% sort
                                
        setcolorder( BASE_FINALE, c(select_variables[1], colonnes_order ))
    }

    
    BASE_FINALE
}
