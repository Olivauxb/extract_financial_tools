fun_pdf_import = function(i, LISTE_TOTAL ){
# print(i)
# i = pdf_list[45]
# i = "010_WB_2018_The-World-Bank-Annual-Report-2018.pdf"  %>% file.path( config$data,. ); x = 5466
# LISTE_MOT = LISTE_TT_ASSET 
# LISTE_MOT = LISTE_EQUITY
# LISTE_TOTAL = LISTE_TOTAL_MOTS
    print(basename(i))
  
    EXPORT_INFO = NULL
    # pdf_file      = file.path(config$data, pdf_list[i])
    document_text = i %>% pdf_text() %>% strsplit("\n")
    # document_text  <- str_split(i[[1]], "\n")
    
    # Nombre de ligne max par page
    longueur_max  = document_text %>% sapply(function(x){length(x)}) %>% max

    # Test si le document n'est pas vide (en gros n'est pas une image)
    if( longueur_max > 0 ){
  
      document = document_text %>% 
                 seq %>% 
                 lapply( function(x){
                                      data.table(  NOM_FICHIER = basename(i)
                                                 , NUM_PAGE    = x
                                                 , TEXT        = document_text[[x]] %>% toupper
                                                 , stringsAsFactors = FALSE  ) }) %>%
                 rbindlist
      # Ajout du numéro de ligne pour le ra 009 avec plusieurs total assets sur la même page et plusieurs table
      document[,":="(NUM_LIGNE = 1:.N)]
    
      
      for( LISTE_MOT in LISTE_TOTAL ){
          INDICE_MOT = NULL
          NUM_INDICE = 1
          while ( length(INDICE_MOT) == 0 & NUM_INDICE <= length(LISTE_MOT)  ){
              MOT = LISTE_MOT[NUM_INDICE]
             
              #INDICE_MOT = document$TEXT %>% grep( MOT,. )
              INDICE_MOT = document$TEXT %>% str_which( MOT )
               
              # INDICE_MOT = document$TEXT %>% str_replace_all(" ","" ) %>% grep( str_replace_all(MOT," ",""),. )
    
              NUM_INDICE = NUM_INDICE + 1
          }
          
          # test tableau bouclé sur j
          if( length( INDICE_MOT ) > 0 ){
                 print(MOT)
                 EXPORT_INFO1 = lapply(  INDICE_MOT
                                      , function(x){ 
                                                      EXPORT_DATA = NULL
                                                      print(x)
                                                      # indice <<-x
                                                      # df1 <<- document
                                                      # MOT1 <<- MOT
                                                      # test si les données sont dans un tableau
                                                      df_t = document[NUM_PAGE == document[x]$NUM_PAGE]
                                                      
                                                      TEST_1 = df_t[ NUM_LIGNE == x]$TEXT %>% str_sub( str_locate(., MOT)[,2]+1, nchar(.)) %>% str_replace_all("^\\s+|\\s+$", "") %>% paste0("  ") %>% fun_test_si_phrase()
                                                      TEST_2 = df_t[ NUM_LIGNE == x]$TEXT %>% str_sub( str_locate(., MOT)[,2]+1, nchar(.)) %>% str_replace_all("^\\s+|\\s+$", "") %>% paste0("  ")%>% fun_test_si_phrase(FALSE)
                                                      TEST_3 = df_t[ NUM_LIGNE %in% c(x-1,x+1)]$TEXT %>% fun_test_si_phrase(FALSE)
                                                      
                                                      # lignes_a_tester = document[ -1:1 + x ][NUM_PAGE == document[x]$NUM_PAGE]$TEXT
                                                      # TEST_SI_PHRASE = fun_test_si_phrase( lignes_a_tester )
                                                      # 
                                                      # # Si dans un tableau on lance la fonction récupération à partir d'un tableau
                                                      # if(TEST_SI_PHRASE) EXPORT_DATA = fun_import_tableau( document, x, MOT )
                                                      
                                                      # Si dans un tableau on lance la fonction récupération à partir d'un tableau
                                                      if( ( TEST_1 & TEST_3 )| TEST_2 ) EXPORT_DATA = fun_import_tableau( document, x, MOT )
 
                                                      
                                                      EXPORT_DATA
                                                      
                                                    } )%>% rbindlist()
                 
                 if (!is.null(EXPORT_INFO1)){
                   EXPORT_INFO[[length(EXPORT_INFO)+1]] = EXPORT_INFO1
                 }
    
          }
      
      }
  }
  EXPORT_INFO %>% rbindlist()
}