fun_import_tableau_old = function(df, indice, MOT){
    # df = document[ NUM_PAGE == document[INDICE_MOT[j]]$NUM_PAGE ,]
    # indice = INDICE_MOT[j]
    # MOT = "TOTAL ASSETS"
    table1_0 = df$ TEXT

    # country_name <- table1[1] %>% 
    #       stringr::str_squish() %>% 
    #       stringr::str_extract(".+?(?=\\sTotal)")
    
    # table_start <- stringr::str_which(table, "Prevalence of diabetes")
    # Indentification de la ligne contenant le total asset et suppression de tous ce qu'il y a après ?
    table_end <- stringr::str_which( table1_0, MOT )

    table1 <- table1_0[1:(table_end)]
    
    table1 <- str_replace_all( table1, "\\s{2,}", "|") %>% str_replace_all("^\\|","")
       
    table2 = mapply( function(x,y) paste(x,y, sep = "|"), x = seq(length(table1)),y = table1)
        
    nb_sep = table2 %>% str_count("\\|")
    table2 = table2[  nb_sep == max(nb_sep)] 
        
    text_con <- textConnection(table2)
    data_table1 <- read.csv(text_con, sep = "|", header =FALSE) %>% setDT
    # names(data_table1)[1] = "NUM"
    
    data_table2 = data_table1[, -"V2"]
    # Un recherche si annee, monnaie et unité dans la tableau sinon plus haut dans la page
    # TROUVER_UNITE = table1_0 %>%
    #                 sapply(function(x){x %>% str_detect(paste(LISTE_UNITE, collapse = "|")) %>% sum}) %>%
    #                 `>`(0) %>%
    #                 which
    
    # Recherche de l'unité
    TROUVER_UNITE = table1_0 %>% str_which( paste( LISTE_UNITE, collapse = "|") ) %>%
                    .[. <= table_end]
    
    TROUVER_UNITE_TABLE = TROUVER_UNITE %>% .[. %in% data_table1$V1]
    
    if( length(TROUVER_UNITE_TABLE) > 0 ){
      EXTRAIRE_UNITE = data_table2[ V1 == max(TROUVER_UNITE_TABLE), ][,-"V1"]%>% t %>% as.vector
    }else{
      EXTRAIRE_UNITE = table1_0[ max(TROUVER_UNITE) ] %>% str_extract( paste( LISTE_UNITE, collapse = "|") ) %>% .[!is.na(.)]
    }
    
    # Recherche monnaie
    TROUVER_MONNAIE = table1_0 %>% str_which( paste( LISTE_MONNAIE, collapse = "|") ) %>%
                    .[. <= table_end]
    
    TROUVER_MONNAIE_TABLE = TROUVER_MONNAIE %>% .[. %in% data_table1$V1]
    if( length(TROUVER_UNITE_TABLE) > 0 ){
        EXTRAIRE_MONNAIE = data_table2[ V1 == max(TROUVER_MONNAIE_TABLE), ][,-"V1"]%>% t %>% as.vector
    }else{
        EXTRAIRE_MONNAIE = table1_0[ max(TROUVER_MONNAIE) ] %>% str_extract( paste( LISTE_MONNAIE  , collapse = "|") ) %>% .[!is.na(.)]
    }
    
    # Recherche année
    TROUVER_ANNEE = table1_0 %>% str_which("(31.?12.?)?20[0-4][0-9]|(31)?.?DEC.*(20)?[0-9]{2}")%>%
                      .[. <= table_end]
    
    TROUVER_ANNEE_TABLE = TROUVER_ANNEE %>% .[. %in% data_table1$V1]
    
    if( length(TROUVER_ANNEE_TABLE) > 0 ){
        EXTRAIRE_ANNEE = data_table2[ V1 == max(TROUVER_ANNEE_TABLE), ][,-"V1"] %>% t %>% as.vector
    }else{
        EXTRAIRE_ANNEE = table1_0[ max(TROUVER_ANNEE) ] %>% str_extract( "(31.?12.?)?20[0-4][0-9]|(31)?.?DEC.*(20)?[0-9]{2}" ) %>% .[!is.na(.)]
    }
    
    EXPORT_INFO = data.table(  NOM_FICHIER = df$NOM_FICHIER[1]
                             , TYPE        = MOT
                             , NUM_PAGE    = df$NUM_PAGE[1]
                             , NUM_LIGNE   = indice
                             , ANNEE       = EXTRAIRE_ANNEE
                             , VALEUR      = data_table2[ V1 == table_end ][,-"V1"] %>% t %>% as.vector %>% fun_mnt_num
                             , UNITE       = EXTRAIRE_UNITE
                             , MONNAIE     = EXTRAIRE_MONNAIE
                             )
  EXPORT_INFO
}  
  # ajouter supprimer les lignes qui n'ont pas de montant