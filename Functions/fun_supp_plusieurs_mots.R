fun_supp_plusieurs_mots = function( df_0, MOT, req_monnaie, req_date, req_unite, req_group  ){
    # df_0 = table1
    liste_indice_supp = NULL
    
    df = df_0
    
    indice_m = df %>% stringr::str_which( req_monnaie )
    # indice   = df %>% stringr::str_which( paste( req_monnaie, req_date, req_unite, "GROUP {2,}(CORPORATION|BANK)|(CORPORATION|BANK) {2,}GROUP", sep = '|' ) )
    indice   = df %>% stringr::str_which( paste( req_monnaie, req_date, req_unite, req_group, sep = '|' ) )
    
    
    indice_plusieurs_mot = df %>% stringr::str_which( MOT ) %>% rev %>% .[-1]
    
    if ( length( indice_plusieurs_mot ) > 0 ){
      
        if( length( indice_m ) > 0 ) indice_plusieurs_mot = indice_plusieurs_mot %>% .[ !. %in% indice_m ]
      
        if ( length( indice_plusieurs_mot ) > 0 ){
          
            liste_indice_supp = indice_plusieurs_mot %>% max %>% seq
            
            if ( length( indice  ) > 0 ) liste_indice_supp = liste_indice_supp %>% .[ !. %in% indice ]
            
        }
    }
    
    # RA 21 (p92) : Ajout on garde unite et annee 
    if ( length( liste_indice_supp ) > 0 ) df = df[-liste_indice_supp]
    
    df
}