fun_pdf_export_train = function( i, LISTE_TOTAL = LISTE_TOTAL_MOTS , PAGES_A_EXCLURE = PAGES_TITRES_A_EXCLURE ){
# i = "CN19941_CDB_2018_dd2018U.pdf" %>% file.path( config$data, . )  ;
# LISTE_MOT = LISTE_TOTAL_MOTS[[1]] 
# LISTE_MOT = "BALANCE SHEET TOTAL"
# LISTE_MOT = LISTE_EQUITY
# LISTE_MOT =LISTE_EXPENSE
# LISTE_TOTAL = LISTE_TOTAL_MOTS
  tt = tryCatch( {
    
    print(basename(i))
  
    EXPORT_INFO = NULL
    
    document_text = i %>% pdf_text() %>% strsplit("\n")
    
    # document_text = document_text %>% str_split("\n")
    # Nombre de ligne max par page
    longueur_max = document_text %>% sapply(function(x){length(x)}) %>% max

    # Test si le document n'est pas vide (en gros n'est pas une image)
    if( longueur_max > 0 ){

      document = document_text %>%
                 seq %>%
                 lapply( function(x){
                                      data.table(  NOM_FICHIER = basename(i)
                                                 , NUM_PAGE    = x
                                                 , TEXT        = document_text[[x]] %>% toupper
                                                 , stringsAsFactors = FALSE  ) }) %>%
                 rbindlist
      # Ajout du numéro de ligne pour le ra 009 avec plusieurs total assets sur la même page et plusieurs table
      document[,":="(NUM_LIGNE = 1:.N)]


      for( LISTE_MOT in LISTE_TOTAL ){
          # LISTE_MOT = LISTE_TOTAL[[1]]
          # INDICE_MOT = NULL
          # NUM_INDICE = 1
          # while ( length(INDICE_MOT) == 0 & NUM_INDICE <= length(LISTE_MOT)  ){
              # MOT = LISTE_MOT[NUM_INDICE]
              MOT = paste0( LISTE_MOT, collapse='|')
              INDICE_MOT = document$TEXT %>% grep( MOT, . )
              # INDICE_MOT = document$TEXT %>% str_which( MOT )
              # NUM_INDICE = NUM_INDICE + 1
          # }
         

          # test tableau bouclé sur j
          if( length( INDICE_MOT ) > 0 ){
                
            

                 EXPORT_INFO1 = document[ NUM_PAGE %in% document[ INDICE_MOT ]$NUM_PAGE , ]

                 if (!is.null(EXPORT_INFO1)){
                   EXPORT_INFO[[length(EXPORT_INFO)+1]] = EXPORT_INFO1
                 }

          }

      }
  }
  EXPORT_INFO %>% rbindlist()
  }
,error=function(e) print(paste("error sur ", i)))
  # if(is(tt,"error")) { print(paste("error sur ", i))
  # }else{  tt
  #   
# }
 
}  
  
  
  