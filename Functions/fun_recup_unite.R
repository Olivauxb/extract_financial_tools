fun_recup_unite = function( df1, df2, df3, df4, req, VALEUR, EXTRAIRE_MONNAIE = NULL, page_avant =NULL ){
  # page_avant = df1[ NUM_PAGE == (df1[NUM_LIGNE == indice,]$NUM_PAGE-1) ,];df4= df2; df1 = table1_0c; df2 = df; df3 = data_table1;  req = req_monnaie; EXTRAIRE_MONNAIE = NULL
  # df1 = table1_0c; df2 = df; df3 = data_table1; req = req_unite
  #  EXTRAIRE_MONNAIE = fun_recup_unite( table1_0c, df, data_table1, df2, req_monnaie, VALEUR, page_avant = df1[ NUM_PAGE == (df1[NUM_LIGNE == indice,]$NUM_PAGE-1) ,])
  
  TEST_MONNAIE = EXTRAIRE_MONNAIE %>% str_detect( req ) %>% any %>% isTRUE
  
  
  TROUVER_UNITE = coalesce( c(   df1[ str_detect( TEXT , req ) ]$NUM_LIGNE
                               , df2[NUM_LIGNE<= max(df1$NUM_LIGNE)][ str_detect( TEXT, req )  ]$NUM_LIGNE )) %>% max(0, na.rm=TRUE)

  # TEST = list(  df1[ str_detect( TEXT , req ) ]
  #      , df2[NUM_LIGNE<= max(df1$NUM_LIGNE)][ str_detect( TEXT, req )  ]) %>% rbindlist()
  # TEST = TEST %>% unique
  # TEST[,':='(ORDRE = TEXT %>% str_detect("AMOUNTS IN"))]
  # setorder(TEST, -ORDRE, -NUM_LIGNE)
  # 
  # TROUVER_UNITE = TEST[1]$NUM_LIGNE
    
  if (TROUVER_UNITE == 0 ){
    indice_min = df2[ str_detect( TEXT, req )  ]$NUM_LIGNE
    if ( length(indice_min)>0)
        TROUVER_UNITE = min( indice_min, na.rm=TRUE)
  }
                               # , df2[ str_detect( str_replace_all(TEXT, "ISSUED EURO BONDS", "") , req )  ]$NUM_LIGNE )) %>% max(0, na.rm=TRUE)

  TROUVER_UNITE_TABLE = sapply( names(df3)[-1] 
                                , function(x){ df3[ str_which( str_replace_all(df3[[x]], "\\.", " "), req ),]$V1 }) %>% 
                        unlist %>%
                        max(0,na.rm=  TRUE)
  
  req_amount = paste0("(AMOUNTS.*|IN )(", req, ')' )
  detect_unite_fin_page   = df2$TEXT %>% .[length(.)] %>% str_detect( req_amount ) %>% isTRUE
  detect_unite_debut_page = df2$TEXT %>% .[1:5]       %>% str_detect( req_amount ) %>% any %>% isTRUE
  
  # df2$TEXT %>% .[1:5]  %>% str_detect( "THOUSANDS OF EURO")
  
  # "(MILLIONS?|THOUSANDS?) EURO?S?"
  
  EXTRAIRE_UNITE = NA_character_
  
  if       ( TEST_MONNAIE            ){ EXTRAIRE_UNITE = EXTRAIRE_MONNAIE
  }else if ( detect_unite_debut_page ){ EXTRAIRE_UNITE = df2$TEXT %>% .[1:5]  %>% str_extract( req_amount ) %>% .[!is.na(.)] %>% head(1)
  }else if ( TROUVER_UNITE_TABLE > 0 ){ EXTRAIRE_UNITE = df3[ V1 == TROUVER_UNITE_TABLE, ][,-"V1"] %>% t %>% as.vector %>% str_replace_all( "\\.", " ") %>% str_subset( req )
  }else if ( detect_unite_fin_page   ){ EXTRAIRE_UNITE = df2$TEXT %>% tail(1) %>% str_extract( req_amount )
  # }else if ( detect_unite_debut_page ){ EXTRAIRE_UNITE = df2$TEXT %>% .[1:5]  %>% str_extract( paste0("AMOUNTS.*(", req, ')' )) %>% head(1)
  }else if ( TROUVER_UNITE > 0       ){ 
        EXTRAIRE_UNITE = df2[ NUM_LIGNE == TROUVER_UNITE ]$TEXT %>% str_replace_all("\\s+", " ") %>% str_replace_all("^\\s+|\\s+$", "")
        if ( nchar(EXTRAIRE_UNITE) > 80) EXTRAIRE_UNITE = NA_character_
  }else if (!is.null(page_avant)){
        EXTRAIRE_UNITE = page_avant$TEXT %>% str_extract( req) %>% .[!is.na(.)] %>% tail(1)
  }
  
    
  if (isTRUE(is.na(EXTRAIRE_UNITE))){
    TROUVER_UNITE = df4[NUM_LIGNE<= max(df1$NUM_LIGNE)][ str_detect( TEXT, req )  ]$NUM_LIGNE %>% max(0, na.rm=TRUE)
    if (TROUVER_UNITE >0)
      EXTRAIRE_UNITE = df4[ NUM_LIGNE == TROUVER_UNITE ]$TEXT %>% str_replace_all("\\s+", " ") %>% str_replace_all("^\\s+|\\s+$", "")
  }
  # RA 52 oeb p18 : pb unité sur une seule colonne et pas l'autre
  # 
  NB_UNITE  = length( EXTRAIRE_UNITE )
  NB_VALEUR = length( VALEUR )

  if       ( NB_UNITE == 1        ) { if ( isTRUE(str_detect( EXTRAIRE_UNITE, "AND AMORTIZATION .EBITDA")) ){ EXTRAIRE_UNITE = NA_character_
                                                                                                       } else EXTRAIRE_UNITE = EXTRAIRE_UNITE[1]
  }else if ( NB_UNITE < NB_VALEUR ) { EXTRAIRE_UNITE = c( EXTRAIRE_UNITE, rep(NA_character_, NB_VALEUR - NB_UNITE))
  }else if ( NB_UNITE > NB_VALEUR & isTRUE(max(df1$NUM_LIGNE)==  TROUVER_UNITE) ) { EXTRAIRE_UNITE = EXTRAIRE_UNITE [ seq(length(EXTRAIRE_UNITE)) %>% .[.> NB_VALEUR]]
  }else if ( NB_UNITE > NB_VALEUR ) { EXTRAIRE_UNITE = NA_character_
  }
  
  EXTRAIRE_UNITE
}
