fun_decalage_group = function( df, req_group = "(GROUP\\|(CORPORATION|BANK))|((CORPORATION|BANK)\\|GROUP)"){
  table3 = df
  
  if( table3 %>% str_detect(req_group) %>% any ){
      table3[ table3 %>% str_detect(req_group) ] = table3 [ table3 %>% str_detect(req_group) ] %>% str_replace("\\|", "||") 
  }
  
  table3 
}