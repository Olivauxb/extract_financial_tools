fun_test_pas_en_colonne = function(df_t, x){
  
  D1 = df_t[NUM_LIGNE %in% x         ]$TEXT %>% str_replace_all("^\\s+|\\s+$", "") %>% str_split('\\s{2,}') %>% sapply(length)
  D2 = df_t[NUM_LIGNE %in% c(x-1,x+1)]$TEXT %>% str_replace_all("^\\s+|\\s+$", "") %>% str_split('\\s{2,}') %>% sapply(length)
  
  isTRUE(D1 == 1 & any(D1< D2))
   
}