
# Attention vérifier si il y a des mixte entre les chiffres ?
fun_mnt_num = function(mnt){
  # mnt = c("62,062.89", "68,527.77")
  # mnt = c("2,633.56", "0.09-",    "2,633.65")
  # mnt = c( "485.8", "472.2")
  # mnt = c("-4,5, 6)",   "35,676,739", "34,738,139")
  
  #Initialisation
  SEP_VIRGULE = FALSE
  
  # suppression des espaces
  mnt2 = mnt %>% str_replace("[:alpha:]","" ) %>% str_replace(" ","") %>% str_replace("-$","")
  mnt2[is.na(mnt2)] = ""
  # mnt2 = mnt2[1:2]
  # mnt2 = paste0(mnt2 , collapse='')
  
  test_val     = mnt2 %>% str_split("\\.") 
  lon_test_val = test_val %>% sapply(length)
  
  if( min( lon_test_val ) >3 ){
      if ( min( lon_test_val ) == max( lon_test_val ) ){
          first_element = test_val %>% lapply(function(x)x[1]) %>% unlist
          nb_char = nchar(first_element)
          
          if ( n_distinct (nb_char) > 1 ){
              if ( any(nb_char %in% 1 ) & any(nb_char > 4 )){
                
                  for(i in seq(test_val) ){
                    test_val[[i]][1] = test_val[[i]][1] %>% str_extract('[0-9]$')
                  }
                  mnt2 = test_val %>% sapply(function(x) paste(x, collapse= "."))
              }
          }
      }
  }
  
  # Test de la virgule
  SI_VIRGULE = mnt2 %>% str_detect(",") %>% any
  
  if (SI_VIRGULE) {
  
    mnt2b = mnt2 %>% str_replace("\\..*", "")
  
    SEP_VIRGULE = mnt2b %>%
                  str_split(",")  %>%
                  lapply( function(x) x[-1]) %>%
                  sapply( function(x){nchar(gsub("\\(|\\)","",x)) == 3}) %>%
                  sapply( function(x) c(x,rep(NA,(max(sapply(. , length)))-length(x)))) %>%
                  unlist %>% 
                  any(na.rm = TRUE)
 
    if ( SEP_VIRGULE ) { mnt2 = mnt2 %>% str_replace_all(",","") 
    }else {              mnt2 = mnt2 %>% str_replace(",","\\;") }
    
  }
    
    # Test du point
    SI_POINT = mnt2 %>% str_detect("\\.") %>% any
    
    if (SI_POINT) {
    
      mnt2b = mnt2 %>% str_replace("(,|;).*", "")
    
      SEP_POINT = mnt2b %>%
                    str_split("\\.")  %>%
                    lapply(function(x) x[-1]) %>%
                    sapply(function(x){nchar(gsub("\\(|\\)","",x)) == 3}) %>%
                    sapply( function(u) c(u,rep(NA,(max(sapply(. , length)))-length(u)))) %>%
                    unlist %>% 
                    any(na.rm = TRUE)
   
      if ( SEP_POINT & !SEP_VIRGULE ) mnt2 = mnt2 %>% str_replace_all("\\.","") 
  
    }
    mnt2 = mnt2 %>% str_replace_all(';|,', '.')%>% str_replace_all(' ', '')
    as.numeric(mnt2)
}