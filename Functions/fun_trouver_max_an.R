fun_trouver_max_an = function ( table1, req_date ){
  
  detect_list_annee = table1 %>% 
                      str_extract_all(req_date) %>% 
                      lapply( function(x) { x %>% 
                                            str_extract("20[0-9]{2}|[0-9]{2}$") %>% 
                                            str_pad(3,pad ="0") %>% 
                                            str_pad(4,pad ="2") %>% 
                                            as.numeric 
                      })
  
  # detect_list_annee[  (detect_list_annee > (year(Sys.Date() + 4 ))) %>% sapply(isTRUE)] = 0
  detect_list_annee = detect_list_annee %>% sapply(function(x){x [ x < (year(Sys.Date()) + 4 )] }) 
  
  ANNEE_MAX = detect_list_annee %>% unlist %>% max(0, na.rm = TRUE)
  
  detect_max_annee = detect_list_annee %in% ANNEE_MAX
  
}