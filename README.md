# Extract financial tools

## Installation

### Softwares 

Install R version 4.0.2 : 
https://cran.r-project.org/

Install RStudio Desktop version 1.2.5042 : 
https://www.rstudio.com/products/rstudio/download/#download


### Packages

Install the package renv version 0.14.0 :
https://cran.r-project.org/web/packages/renv/index.html

In RStudio run :
```
renv::init()
renv::restore()
```

