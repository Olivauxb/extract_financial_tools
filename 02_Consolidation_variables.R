corpus_raw_tt = config$data_inter %>% list.files(pattern = "corpus_raw_tt_20", full.names = TRUE) %>% max %>% read_rds

liste_db = list.files(config$input, full.names = TRUE, pattern = "Data_base.*_2018_.*xlsx") %>% str_subset("\\$", negate = TRUE)

DATA_BASE_PDB =  read_excel( max(liste_db),skip =1) %>% 
                        setDT %>%
                        .[ !is.na(Nb) ]

setnames(DATA_BASE_PDB, names(DATA_BASE_PDB), make.names(names(DATA_BASE_PDB)))
setnames(DATA_BASE_PDB, "Code.Bank", "CODE_BANK")
# setnames(DATA_BASE_PDB, "N°", "NUM")

nom_var = names(DATA_BASE_PDB) %>% str_subset("USD|Employees|LC")

lapply( nom_var 
        , function( x ) {
          if ( x %in% names(DATA_BASE_PDB) ) {
            "DATA_BASE_PDB[, {x} := as.numeric({x}) ]" %>% str_glue %>% parse(text=.) %>% eval(envir= .GlobalEnv)
          }
          
          })

corpus_raw = corpus_raw_tt  %>% .[sapply(.,is.data.frame)] %>% rbindlist

# CONSO MONNAIE ET UNITE
corpus_raw[ ,':='(  CODE_BANK   = NOM_FICHIER %>% word(1, sep = "_")
                  # , ANNEE       = DATE %>% str_extract("20[0-9]{2}|[0-9]{2}$") %>% str_pad(3,pad ="0") %>% str_pad(4,pad ="2") %>% as.numeric
                  , ANNEE_RA    = NOM_FICHIER %>% word(3, sep= "_") %>% str_sub(1,4)
                  , MONNAIE_ORI = str_replace_all( MONNAIE_EXT, "^\\s+|\\s+$", "")
                  )]

corpus_raw [   nchar(str_replace_all( UNITE_ORI, "-", "")) > 75 
            & !str_detect(UNITE_ORI,"AMOUNT|EXPRESSED|TABLE"), ":="( UNITE_ORI = NA_character_) ]

corpus_raw [ ,':='( MONNAIE = str_extract( MONNAIE_ORI, paste0(LISTE_MONNAIE, collapse = "|")))]

corpus_raw [ UNITE_ORI %>% str_detect( "CRORE"           ) & ! MONNAIE %>% str_detect( "CRORE" ),':='( MONNAIE = UNITE_ORI        )]
corpus_raw [ UNITE_ORI %>% str_detect( "ARGENTINE.PESOS" ) &   MONNAIE %>% str_detect( "\\$"   ),':='( MONNAIE = "ARGENTINE PESOS")]

# Conso Monnaie
corpus_raw [ MONNAIE_ORI %>% str_detect("\\$1,000"), MONNAIE_ORI := "\\$"]

corpus_raw [,":="( UNITE = case_when(   MONNAIE_ORI %>% str_detect( "BILL?I?ON|SKR BN|DKKBN|€BLN"                   ) ~ "BILLIONS"
                                      , MONNAIE_ORI %>% str_detect( "100 MILLION"                                   ) ~ "100 MILLIONS"
                                      , MONNAIE_ORI %>% str_detect( "CRORE"                                         ) ~ "10 MILLIONS"
                                      , MONNAIE_ORI %>% str_detect("NOK ?(1,?|') ?000|MILLION|\\$M|SKR MN|1 000 000|M LL ON|€.MLN|MEUR|IIUF ,;tiltion|MILLONE|CHFM|CZKM|V MIL  KČ|JUTAAN RUPIAH|RONM|IN MN OF| MN,|MIL\\.|SEK M|MDH") ~ "MILLIONS"
                                      , MONNAIE_ORI %>% str_detect("(’|‘)000( |$)|(N ?)?'000( |$)|THOUSAND|MILLIERS?|MILE|000’|1000S|EUR X 1,000|EUR K|EUR 1,000| 000( |$)|MILES|MBIF|1,000|£000|EUR..?000|€ .000S|\\$ 000|RON.000|€000|KDH"  ) ~ "THOUSANDS"
                                      
                                      , UNITE_ORI %>% str_detect( "BILL?I?ON|SKR BN|DKKBN"                          ) ~ "BILLIONS"
                                      , UNITE_ORI %>% str_detect( "100 MILLION"                                     ) ~ "100 MILLIONS"
                                      , UNITE_ORI %>% str_detect( "CRORE"                                           ) ~ "10 MILLIONS"
                                      , UNITE_ORI %>% str_detect( "€ M|MILLION|£M|DKKM|\\$M|SKR MN|MEUR|UN T. N M LL ON|€.MLN|MEUR|IIUF ,;tiltion|MILLONE|CHFM|CZKM|JUTAAN RUPIAH|RONM|IN MN OF|MIL\\.|MDH") ~ "MILLIONS"
                                      # , UNITE_ORI %>% str_detect( "’000|N'000|THOUSAND|MILLIERS|MILE|000’|1000S|EUR X 1,000|EUR K|EUR 1,000|000|MILES") ~ "THOUSANDS"
                                      , UNITE_ORI %>% str_detect( "(’|‘)000( |$)|(N ?)?'000( |$)|THOUSAND|MILLIERS?|MILE|000’|1000S|EUR X 1,000|EUR K|EUR 1,000| 000( |$)|MILES|MBIF|1,000|£000|EUR..?000|€ .000S|\\$ 000|€000|KDH") ~ "THOUSANDS"
                                      , TRUE ~ NA_character_ 
                                      ))]

# corpus_raw$UNITE %>% table(useNA = "always") %>% t %>% t
# analyse_unite = corpus_raw[is.na(UNITE)]
# corpus_raw[(!is.na(UNITE)) & (VALEUR>10000000000), ]

corpus_raw[!is.na(UNITE) & (VALEUR>10000000000), UNITE:= NA_character_]

# Ajout Database + devise
corpus_raw2 = corpus_raw %>% 
              merge( DATA_BASE_PDB, by = "CODE_BANK", all.y = TRUE) %>%
              merge( PAYS_DEVISE, by.x = "Country", by.y = "PAYS", all.x = TRUE)

setnames(corpus_raw2, names(corpus_raw2), toupper(names(corpus_raw2)))

corpus_raw2[, ':='( DEVISE_C = case_when(  MONNAIE %>% str_detect("RENMINBI|RMB") ~ "CNY"
                                      , MONNAIE %>% str_detect("^RM")              ~ "MYR"
                                      , MONNAIE %>% str_detect("RONM")              ~ "RON"
                                      , MONNAIE %>% str_detect("KWANZA|(^| )KZ( |$)") ~ "AOA"
                                      , MONNAIE %>% str_detect("CANADIAN DOLLARS")
                                      | COUNTRY == "Canada"                        ~ "CAD"
                                      , COUNTRY == "Pakistan"                      ~ "PKR"
                                      , COUNTRY == "Samoa"                          ~ "WST"
                                      , MONNAIE %>% str_detect("FRW")      ~ "RWF"
                                      , MONNAIE %>% str_detect("K.000|KWACHA")      ~ "ZMW"
                                      , MONNAIE %>% str_detect("BIF")      ~ "BIF"
                                      , MONNAIE %>% str_detect("BIRR")      ~ "ETB"
                                      , MONNAIE %>% str_detect("ZWL")                   ~ "ZWD"
                                      , MONNAIE %>% str_detect("RUPEE|CRORE")      ~ "INR"
                                      , MONNAIE %>% str_detect("ARGENTINE.?PESOS") ~ "ARS"
                                      , MONNAIE %>% str_detect("REAIS|R\\$")        ~ "BRL"
                                      , MONNAIE %>% str_detect("NOK")              ~ "NOK"
                                      , MONNAIE %>% str_detect("PESO")             ~ DEVISE
                                      , MONNAIE %>% str_detect("€|EUR")            ~ "EUR"
                                      , MONNAIE %>% str_detect("¥|YEN")            ~ "JPY"
                                      , MONNAIE %>% str_detect("£")                ~ "GBP"
                                      , MONNAIE %>% str_detect("BAHT")             ~ "THB"
                                      , MONNAIE %>% str_detect("RAND|R[0-9]+")     ~ "ZAR"
                                      , MONNAIE %>% str_detect("RP|RUPIAH")               ~ "IDR"
                                      , MONNAIE %>% str_detect("TL|TURKISH LIRA|TRL")       ~ "TRY"
                                      , MONNAIE %>% str_detect("N\\$" ) ~ "NAD"
                                      , MONNAIE %>% str_detect("ISLAMIC DINAR|UA MILLION" ) ~ "XDR"
                                      , MONNAIE %>% str_detect("QATARI RIYALS")             ~ "QAR"
                                      , MONNAIE %>% str_detect("RUSSIAN ROUBLES")           ~ "RUB"
                                      , MONNAIE %>% str_detect("PLN")                       ~ "PLN"
                                      , MONNAIE %>% str_detect("CHILEAN.?PESOS|MCH\\$")
                                        |(COUNTRY == "Chile" &  str_detect(MONNAIE,"MM\\$"))   ~ "CLP"
                                      
                                      , MONNAIE %>% str_detect("\\$") & COUNTRY =="Mexico"  ~ "MXN"
                                      , MONNAIE %>% str_detect("\\$|U.S. DOLLARS|COP") & COUNTRY =="Colombia" ~ "COP"
                                      , MONNAIE %>% str_detect("DOLLARS|\\$") ~ "USD"
                                      , MONNAIE %>% str_detect("SKR|SEK")         ~ "SEK"
                                      , MONNAIE %>% str_detect("DKK")         ~ "DKK"
                                      , MONNAIE %>% str_detect("HUF|IIUF ,;tiltion")   ~ "HUF"
                                      , MONNAIE %>% str_detect("NAIRA")       ~ "NGN"
                                      , MONNAIE %>% str_detect("SOLES")       ~ "PEN"
                                      , MONNAIE %>% str_detect("CZK|V MIL  KČ")        ~ "CZK"
                                      , MONNAIE %>% str_detect("BDT|TAKA")             ~ "BDT"
                                      , MONNAIE %>% str_detect("F.?CFA")                ~ "XOF"
                                      , MONNAIE %>% str_detect("TZS")                  ~ "TZS"
                                      , MONNAIE %>% str_detect("UA (MIL|THOU)")        ~ "XDR"
                                      , MONNAIE %>% str_detect("P.000")                ~ "BWP"
                                      , MONNAIE %>% str_detect("BS|BOLIVIANOS")        ~ "BOB"
                                    
                                      , MONNAIE %>% str_detect("VND|UN T. N M LL ON")  ~ "VND"
                                      , MONNAIE %>% str_detect("CHF")                  ~ "CHF"
                                      , MONNAIE %>% str_detect("TENGE")                ~ "KZT"
                                      , MONNAIE %>% str_detect("DH")                   ~ "MAD"
                                      , MONNAIE %>% str_detect("BD THOUSANDS|BAHRAINI DINAR") ~ "BHD"
                                      , MONNAIE %>% str_detect("KUWAITI DINAR") ~ "KWD"
                                      , MONNAIE %>% str_detect("SAUDI RIYALS")~ "SAR"
                                      , MONNAIE %>% str_detect("RO")~ "OMR"
                                    
                                      , TRUE ~ MONNAIE  #%>% str_replace_all("[^A-Z]","")
                                     )
                 )
          ]
# corpus_raw %>% with(table(MONNAIE_ORI, useNA = "always")) %>% t %>% t

corpus_raw2[,":="( DEVISE_CONSO = if_else(!is.na(DEVISE_C), DEVISE_C, DEVISE ) %>% str_replace_all(" ","") )]
# corpus_raw2$DEVISE_CONSO %>% table(useNA = "always") %>% t %>% t
# corpus_raw2[is.na(DEVISE_CONSO)]

# Correction RA sans unité
corpus_raw2[ CODE_BANK == "MX19371" & is.na(UNITE) & nchar(VALEUR)== 6, ":="(UNITE = "MILLIONS")  
          ][nchar(VALEUR) > 9 & str_detect(UNITE, "MILLIONS"), UNITE := NA_character_ ]

corpus_raw2[, ':='( UNITE_NUM = case_when(  UNITE == "THOUSANDS"    ~ 1000
                                          , UNITE == "MILLIONS"     ~ 1000000
                                          , UNITE == "10 MILLIONS"  ~ 10000000
                                          , UNITE == "100 MILLIONS" ~ 100000000
                                          , UNITE == "BILLIONS"     ~ 1000000000
                                          , UNITE == "TRILLIONS"    ~ 1000000000000 
                                          , TRUE ~ 1 ) )]

corpus_raw2[, ':='( MNT =  UNITE_NUM * VALEUR )]


names(corpus_raw2)[duplicated(names(corpus_raw2))] = "OTHER2"


corpus_raw2[, ':='(DATE = DATE %>% str_replace_all("^\\s+|\\s+$", "") %>% str_replace_all("^[^[:alnum:]]|[^[:alnum:]]$", ""))]

corpus_raw2[, DATE_CONSO := case_when(  str_detect( DATE, "1 JAN–31 DEC 20") ~  paste0( "31/12/", str_extract(DATE,"20[0-9]{2}"))
                                      , word(DATE,2) == "JANUARY" ~ str_replace(DATE," JANUARY ", '/01/')
                                      , str_detect( DATE, "JANUARY 1|1. 1. 20[0-9]{2}") ~  paste0( "01/01/", str_extract(DATE,"20[0-9]{2}"))
                                      , str_detect( DATE, "31.MARCH|MARCH.31,.") ~  paste0( "31/03/", str_extract(DATE,"20[0-9]{2}"))
                                      , str_detect( DATE, "30.JUN|JUN.* 30,."  ) ~  paste0( "30/06/", str_extract(DATE,"20[0-9]{2}"))
                                      
                                      , str_detect( DATE, "30.SEP|SEP.* 30,."  ) ~  paste0( "30/09/", str_extract(DATE,"20[0-9]{2}"))
                                      , str_detect( DATE, "31.OCT|OCT.* 30,."  ) ~  paste0( "31/10/", str_extract(DATE,"20[0-9]{2}"))
                                      , str_detect( DATE, "30.NOV|NOV.* [0-9]{2}, 20[0-9]{2}"  ) ~  paste0( "30/11/", str_extract(DATE,"20[0-9]{2}"))
                                      # , str_detect( DATE, ""  ) ~  DATE %>% str_extract("(20)?[0-9]{2}$") %>% str_pad(3,pad ="0") %>% str_pad(4,pad ="2") %>% paste0( "31/12/",.)
                                      , str_detect( DATE, "31.DEC|DEC.*31,.|DEC.?-|^FY[0-9]") ~  DATE %>% str_extract("(20)?[0-9]{2}$") %>% str_pad(3,pad ="0") %>% str_pad(4,pad ="2") %>% paste0( "31/12/",.)
                                      , str_detect( DATE, "20[0-9]{2}/12/31") ~  DATE %>% str_extract("^20[0-9]{2}") %>% paste0( "31/12/",.)
                                      ,   str_detect(DATE, "^(28|29|30|31|01.01.)")
                                        & str_detect( DATE, "20[0-9]{2}$")
                                        & nchar(DATE) == 10 ~ DATE
                                      ,   str_detect( DATE, "12.31.(20)?[0-9]{2}")  ~  DATE %>% str_extract("(20)?[0-9]{2}$") %>% str_pad(3,pad ="0") %>% str_pad(4,pad ="2") %>% paste0( "31/12/",.)
                                      , DATE %>% str_detect("(20)?[0-9]{2}") & str_detect( MOIS_EXTRAIT , "31.MARCH|MARCH.31") ~ DATE %>% str_extract("(20)?[0-9]{2}") %>% str_pad(3,pad ="0") %>% str_pad(4,pad ="2") %>% paste0( "31/03/",.)
                                      , DATE %>% str_detect("(20)?[0-9]{2}") ~ DATE %>% str_extract("(20)?[0-9]{2}") %>% str_pad(3,pad ="0") %>% str_pad(4,pad ="2") %>% paste0( "31/12/",.)
                                    
   )]

corpus_raw2[ , DATE_CONSO := DATE_CONSO %>% str_replace_all( "\\.", "/")]
corpus_raw2[ , DATE_CONSO := dmy(DATE_CONSO)]

# Correction année 01/01
corpus_raw2[ month(DATE_CONSO) == 1 & day(DATE_CONSO)==1, DATE_CONSO := DATE_CONSO -1
          ][ month(DATE_CONSO) == 3, DATE_CONSO := dmy(paste0("31/12/", year(DATE_CONSO)-1))
          ]

corpus_raw2[ ,  ANNEE := DATE_CONSO %>% str_extract("20[0-9]{2}|[0-9]{2}$") %>% str_pad(3,pad ="0") %>% str_pad(4,pad ="2") %>% as.numeric]

# 8 sans devise
corpus_raw3 = corpus_raw2 %>%
              merge( TAUX_DE_CHANGE, by.x = c("DEVISE_CONSO","ANNEE"), by.y = c("CODE_DE_LA_DEVISE","ANNEE"), all.x = TRUE )

corpus_raw3[,":="(   MONTANT_USD            = MNT/UNITES_PAR_USD
          )][,":="(  MONTANT_FINAL          = MONTANT_USD /1000000000
                   , MONTANT_IN_MILLION_USD = MONTANT_USD /1000000 
          )]

summary(corpus_raw3)

# FIN
saveRDS(corpus_raw3, file.path(config$data_inter,str_glue("corpus_raw3_{DATE}.RDS", DATE = str_replace_all(today(), "-", "")) ))
